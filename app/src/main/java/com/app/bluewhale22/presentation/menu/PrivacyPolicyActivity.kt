package com.app.bluewhale22.presentation.menu

import android.annotation.SuppressLint
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import com.app.bluewhale22.BaseActivity
import com.app.bluewhale22.R
import com.app.bluewhale22.databinding.ActivityWebViewBinding

class PrivacyPolicyActivity : BaseActivity() {

    lateinit var binding: ActivityWebViewBinding
    lateinit var U_R_L : String

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_web_view)

        U_R_L = intent.extras?.getString("myUrl")?:"https://www.bluewhale.host"

        binding.imgBackWW.setOnClickListener {
            onBackPressed()
        }

        binding.webView.settings.javaScriptEnabled = true
        binding.webView.loadUrl(U_R_L)
        binding.webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String?): Boolean {
                view.loadUrl(url?:"https://www.bluewhale.host")
                return true
            }
        }
    }

    // if you press Back button this code will work
    override fun onBackPressed() {
        // if your webview can go back it will go back
        if (binding.webView.canGoBack())
            binding.webView.goBack()
        // if your webview cannot go back
        // it will exit the application
        else
            super.onBackPressed()
    }
}