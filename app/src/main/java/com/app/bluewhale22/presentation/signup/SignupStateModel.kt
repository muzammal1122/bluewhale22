package com.app.bluewhale22.presentation.signup

import com.app.bluewhale22.domain.signUp.entity.SignUpApiResponse
import com.app.bluewhale22.presentation.login.LoginStateModel
import java.lang.Exception

sealed class SignupStateModel {
    object Init : SignupStateModel()
    data class IsLoading(val isLoading : Boolean) : SignupStateModel()
    data class SignUpResponse(val signUpApiResponse: SignUpApiResponse) : SignupStateModel()
    data class FoundException(val exception: Exception) : SignupStateModel()
    data class StatusFailed(val message: String) : SignupStateModel()
}