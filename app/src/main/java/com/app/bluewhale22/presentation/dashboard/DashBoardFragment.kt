package com.app.bluewhale22.presentation.dashboard

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.app.bluewhale22.R
import com.app.bluewhale22.databinding.DashBoardFragmentBinding
import com.app.bluewhale22.domain.dashboard.entity.ChartEntity
import com.app.bluewhale22.domain.dashboard.entity.DashBoardApiResponse
import com.app.bluewhale22.utils.Constants
import com.app.bluewhale22.utils.Utils
import com.app.bluewhale22.utils.Utils.numberFormat
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class DashBoardFragment : Fragment() {

    private val TAG = "DashBoardFragment"
    lateinit var binding: DashBoardFragmentBinding
    @Inject
    lateinit var viewModel: DashBoardViewModel
    @Inject
    lateinit var preferences: DataStore<Preferences>
    private var arrayList = ArrayList<ChartEntity>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.dash_board_fragment, container, false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initObserver()
        initLineChart()

        lifecycleScope.launch {
            try {

                binding.txtMarketageDF.text =
                    context?.let { numberFormat((Utils.getDoubleValue(preferences, Constants.MARKET_PRICE).first() ?: 0).toLong() , it) }
            }catch (e : Exception){
                Log.d(TAG, "asas   " +e.message.toString())
            }
            binding.txtPercentageDF.text =  "${Utils.getDoubleValue(preferences, Constants.MARKET_PERCENTAGE).first() ?: 0}%"
        }

        viewModel.getDashBoardData()

        // go to notification screen
        binding.imgNotificationDash.setOnClickListener {
            showToast(getString(R.string.no_notifications))

        }

    }

    private fun initObserver() {
        viewModel.mState.flowWithLifecycle(
            this.lifecycle, Lifecycle.State.STARTED
        ).onEach {
            handleState(it)
        }.launchIn(this.lifecycleScope)
    }

    private fun handleState(state: DashBoardStateModel) {
        when (state) {
            is DashBoardStateModel.IsLoading -> handleIsLoadingState(state.isLoading)
            is DashBoardStateModel.Response -> handleApiResponse(state.response)
            is DashBoardStateModel.GenericError -> handleGenericError(state.message)
            is DashBoardStateModel.StatusFailed -> handleFailure(state.message)
            else -> {
            }
        }
    }

    private fun handleFailure(message: String) {
        showToast(message)
    }

    private fun handleGenericError(message: String) {
        Log.d(TAG, message)
        showToast(message)

    }

    private fun showToast(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    @SuppressLint("SetTextI18n")
    private fun handleApiResponse(response: DashBoardApiResponse) {
        Log.d(TAG, response.market_price.toString())
        Log.d(TAG, response.market_percentage.toString())
        try {

            binding.txtMarketageDF.text =
                context?.let { numberFormat(response.market_price.toLong(), it) }
        }catch (e : Exception){
            Log.d(TAG, "asas   " +e.message.toString())
        }
        binding.txtPercentageDF.text = response.market_percentage.toString() +"%"
        response.market_movement.forEach{
            arrayList.add(ChartEntity(it.key,it.value))
        }
        setDataToLineChart()


        // SAVE VALUES
        lifecycleScope.launch {
            Utils.addDoubleValue(preferences, Constants.MARKET_PRICE, response.market_price)
            Utils.addDoubleValue(preferences, Constants.MARKET_PERCENTAGE, response.market_percentage)
        }

    }

    private fun handleIsLoadingState(loading: Boolean) {
        if (loading)
            Log.d(TAG, "show loader....")
        else
            Log.d(TAG, "..... stop loader")
    }

    private fun initLineChart() {

//        hide grid lines
        binding.lineChart.axisLeft.setDrawGridLines(false)
        val xAxis: XAxis = binding.lineChart.xAxis
        xAxis.setDrawGridLines(false)
        xAxis.setDrawAxisLine(false)

        //remove right y-axis
        binding.lineChart.axisRight.isEnabled = false

        //remove legend
        binding.lineChart.legend.isEnabled = false


        //remove description label
        binding.lineChart.description.isEnabled = false


        //add animation
        binding.lineChart.animateX(1000, Easing.EaseInSine)

        // to draw label on xAxis
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.valueFormatter = MyAxisFormatter()
        xAxis.setDrawLabels(true)
        xAxis.granularity = 1f
        // xAxis.labelRotationAngle = +90f

    }

    inner class MyAxisFormatter : IndexAxisValueFormatter() {

        override fun getAxisLabel(value: Float, axis: AxisBase?): String {
            val index = value.toInt()
            return if (index < arrayList.size) {
                arrayList[index].year
            } else {
                ""
            }
        }
    }

    private fun setDataToLineChart() {
        //now draw bar chart with dynamic data
        val entries: ArrayList<Entry> = ArrayList()



        //you can replace this data object with  your custom object
        for (i in arrayList.indices) {
            val score = arrayList[i]
            entries.add(Entry(i.toFloat(), score.value.toFloat()))
        }

        val lineDataSet = LineDataSet(entries, "")

        // line  color
        lineDataSet.color = activity?.let { ContextCompat.getColor(it, R.color.white) }!!
        // inside color
        lineDataSet.fillColor = activity?.let { ContextCompat.getColor(it, R.color.darkBlue) }!!
        // inside color thickness
        lineDataSet.fillAlpha = 225
        // enable inside color
        lineDataSet.setDrawFilled(true)

        lineDataSet.setDrawCircles(true)

        val data = LineData(lineDataSet)
        binding.lineChart.data = data

        binding.lineChart.invalidate()
    }



}