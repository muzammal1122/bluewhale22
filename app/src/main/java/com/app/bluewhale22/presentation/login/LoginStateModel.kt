package com.app.bluewhale22.presentation.login

import com.app.bluewhale22.domain.login.entity.LoginApiResponse
import com.app.bluewhale22.domain.signUp.entity.SignUpApiResponse
import java.lang.Exception

sealed class LoginStateModel    {

    object Init : LoginStateModel()
    data class IsLoading(val isLoading : Boolean) : LoginStateModel()
    data class LoginResponse(val loginApiResponse: LoginApiResponse) : LoginStateModel()
    data class FoundException(val exception: Exception) : LoginStateModel()
    data class GenericError(val message: String) : LoginStateModel()
    data class StatusFailed(val message: String) : LoginStateModel()


}
