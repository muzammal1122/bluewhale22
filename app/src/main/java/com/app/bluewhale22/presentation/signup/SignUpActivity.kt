package com.app.bluewhale22.presentation.signup

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.app.bluewhale22.BaseActivity
import com.app.bluewhale22.R
import com.app.bluewhale22.databinding.ActivitySignUpBinding
import com.app.bluewhale22.domain.login.entity.LoginApiResponse
import com.app.bluewhale22.domain.signUp.entity.SignUpApiResponse
import com.app.bluewhale22.domain.signUp.entity.SignUpEntity
import com.app.bluewhale22.presentation.referral.ReferralCodeActivity
import com.app.bluewhale22.utils.Constants
import com.app.bluewhale22.utils.Utils
import com.app.bluewhale22.utils.showToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class SignUpActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: SignUpViewModel
    lateinit var binding: ActivitySignUpBinding

    private var phone: String = ""
    val TAG: String = "SignUpActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up)
        // receive data from previous activity
        phone = intent.extras?.getString("phoneCode")!!

        initObserver()

        binding.imgBackSA.setOnClickListener {
            onBackPressed()
        }

        binding.imgBackSA.setOnClickListener { onBackPressed() }

        binding.btnRegisterOtp.setOnClickListener {
            lifecycleScope.launch {
                val token = Utils.getStringValue(preferences, Constants.FCM_TOKEN).first()
                    ?: "Token not found"
                if (token != "Token not found") {
                    if (binding.etEmailSUA.text.isNotEmpty() && binding.etFNameSUA.text.isNotEmpty() && binding.etLNameSUA.text.isNotEmpty()) {
                        viewModel.getSignUpApiResponse(
                            SignUpEntity(
                                binding.etEmailSUA.text.toString(),
                                null,
                                binding.etFNameSUA.text.toString(),
                                binding.etLNameSUA.text.toString(),
                                phone,
                                token
                            )
                        )
                    } else {
                        showToast(getString(R.string.fill_all_fields))

                    }
                }else{
                    showToast("Something goes went wrong")
                }

            }
        }
    }

    private fun initObserver() {
        viewModel.mState.flowWithLifecycle(
            this.lifecycle, Lifecycle.State.STARTED
        ).onEach {
            handleState(it)
        }.launchIn(this.lifecycleScope)
    }

    private fun handleState(state: SignupStateModel) {
        when (state) {
            is SignupStateModel.IsLoading -> handleIsLoadingState(state.isLoading)
            is SignupStateModel.SignUpResponse -> handleApiResponse(state.signUpApiResponse)
            is SignupStateModel.FoundException -> handleException(state.exception)
            is SignupStateModel.StatusFailed -> handleFailure(state.message)
            else -> {
            }
        }
    }

    private fun handleException(exception: Exception) {
        Log.d(TAG, exception.message.toString())
    }

    private fun handleApiResponse(signUpApiResponse: SignUpApiResponse) {
        Log.d(TAG, signUpApiResponse.message)
        saveData(LoginApiResponse( binding.etLNameSUA.text.toString(), binding.etEmailSUA.text.toString(),binding.etFNameSUA.text.toString(), signUpApiResponse.code))
        val intent = Intent(this, ReferralCodeActivity::class.java)
        intent.putExtra("phoneCode", phone)
        intent.putExtra(Constants.USER_TYPE, Constants.NEW_USER)
        startActivity(intent)
    }

    private fun handleIsLoadingState(loading: Boolean) {
        if (loading) {
            Log.d(TAG, "show loader....")
            binding.spinKit.visibility = View.VISIBLE
        }
        else {
            Log.d(TAG, "..... stop loader")
            binding.spinKit.visibility = View.GONE
        }
    }

    private fun handleFailure(message: String) {
        showToast(message)
    }

    private fun saveData(response: LoginApiResponse) {
        lifecycleScope.launch {
            Utils.addStringValue(preferences, Constants.FNAME, response.first_name)
            Utils.addStringValue(preferences, Constants.LNAME, response.last_name)
            Utils.addStringValue(preferences, Constants.EMAIL, response.email)
            Utils.addStringValue(preferences, Constants.REFERRAL_CODE, response.code)
            Utils.addStringValue(preferences, Constants.PHONE, phone)
        }
    }
}