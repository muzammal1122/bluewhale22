package com.app.bluewhale22.presentation.menu.profile

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.app.bluewhale22.R
import com.app.bluewhale22.databinding.ProfileFragmentBinding
import com.app.bluewhale22.domain.menu.entity.ProfileEntity
import com.app.bluewhale22.domain.menu.entity.UpdateProfileResponse
import com.app.bluewhale22.presentation.home.HomeFragment
import com.app.bluewhale22.presentation.menu.MenuFragment
import com.app.bluewhale22.presentation.menu.MenuViewModel
import com.app.bluewhale22.utils.Constants
import com.app.bluewhale22.utils.Utils
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class ProfileFragment : Fragment() {

    @Inject
    lateinit var viewModel: MenuViewModel

    @Inject
    lateinit var preferences: DataStore<Preferences>
    lateinit var binding: ProfileFragmentBinding
    lateinit var phone: String
    var parent = "Menu"
    val TAG: String = "ProfileFragment"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.profile_fragment, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initObserver()

        lifecycleScope.launch {
            val fName = Utils.getStringValue(preferences, Constants.FNAME).first() ?: "--"
            binding.fName = fName
            binding.txtStartNamePF.text = fName[0].toString()
            val lName = Utils.getStringValue(preferences, Constants.LNAME).first() ?: "--"
            binding.lName = lName
            binding.txtFullNamePF.text = "$fName $lName"
            binding.email = Utils.getStringValue(preferences, Constants.EMAIL).first() ?: "--"
            binding.phone = Utils.getStringValue(preferences, Constants.PHONE).first() ?: "--"
            phone = Utils.getStringValue(preferences, Constants.PHONE).first() ?: "--"

        }

        binding.imgBackPF.setOnClickListener {
            val fragment : Fragment = if (parent == "Menu"){
                MenuFragment()
            }else{
                HomeFragment()
            }
            fragmentManager?.commit {
                setReorderingAllowed(true)
                replace(R.id.fragment_container_view, fragment)
            }
        }

        binding.llUpdate.setOnClickListener {
            hideKeyBoard()
            binding.txtFullNamePF.text = "${binding.etFNamePA.text} ${binding.etLNamePA.text}"
            viewModel.getUpdateProfileResponse(
                phone,
                ProfileEntity(
                    binding.etFNamePA.text.toString(),
                    binding.etLNamePA.text.toString(),
                    "dubai"
                )
            )
        }


    }

    private fun initObserver() {
        viewModel.mState.flowWithLifecycle(
            this.lifecycle, Lifecycle.State.STARTED
        ).onEach {
            handleState(it)
        }.launchIn(this.lifecycleScope)
    }

    private fun handleState(state: MenuModelState) {
        when (state) {
            is MenuModelState.IsLoading -> handleIsLoadingState(state.isLoading)
            is MenuModelState.ProfileResponse -> handleApiResponse(state.profileResponse)
            is MenuModelState.FoundException -> handleException(state.exception)
            is MenuModelState.StatusFailed -> handleFailure(state.message)
            else -> {
            }
        }
    }

    private fun handleException(exception: Exception) {
        Log.d(TAG, exception.message.toString())
    }

    private fun handleApiResponse(response: UpdateProfileResponse) {
        Log.d(TAG, "SUCCESS   $response.message")
        binding.spinKit.visibility = View.GONE
        lifecycleScope.launch {
            Utils.addStringValue(preferences, Constants.FNAME, binding.etFNamePA.text.toString())
            Utils.addStringValue(preferences, Constants.LNAME, binding.etLNamePA.text.toString())
        }
        Toast.makeText(activity, response.message, Toast.LENGTH_SHORT).show()
    }

    private fun handleIsLoadingState(loading: Boolean) {
        if (loading) {
            Log.d(TAG, "show loader....")
            binding.spinKit.visibility = View.VISIBLE
        } else {
            Log.d(TAG, "..... stop loader")
            binding.spinKit.visibility = View.GONE
        }
    }

    private fun handleFailure(message: String) {
        Log.d(TAG, "failure    $message")
        binding.spinKit.visibility = View.GONE
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    private fun hideKeyBoard() {
        val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(requireView().windowToken, 0)
    }

}