package com.app.bluewhale22.presentation.login

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.app.bluewhale22.BaseActivity
import com.app.bluewhale22.R
import com.app.bluewhale22.databinding.ActivityPhoneNumberBinding
import com.app.bluewhale22.domain.login.entity.LoginApiResponse
import com.app.bluewhale22.domain.login.entity.LoginEntity
import com.app.bluewhale22.utils.Constants
import com.app.bluewhale22.utils.Utils
import com.app.bluewhale22.utils.showToast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class PhoneNumberActivity : BaseActivity() {


    @Inject
    lateinit var viewModel: LoginViewModel


    private val TAG = "PhoneNumberActivity"
    lateinit var binding: ActivityPhoneNumberBinding
    private var phone: String = ""
    private var loginCode: String = ""
    private lateinit var token: String
    private lateinit var userType: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_phone_number)

        userType = intent.extras?.getString("userType") ?: Constants.NEW_USER

        initObserver()
        lifecycleScope.launch {
            loginCode = Utils.getStringValue(preferences, Constants.Login_KEY).first() ?: "no code"
            Log.d(TAG, "  save login code   $loginCode")
        }

        // country coe picker
        binding.countryCodePicker.setAutoDetectedCountry(true)
        binding.countryCodePicker.isValidFullNumber


        generateFCMToken()

        binding.etPhoneSA.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s?.length!! > 6) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        binding.btnSendOtp.setTextColor(getColor(R.color.white))
                        binding.btnSendOtp.setBackgroundResource(R.drawable.button_bg)
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        binding.btnSendOtp.setTextColor(getColor(R.color.darkBlue))
                        binding.btnSendOtp.setBackgroundResource(R.drawable.phone_number_bg)
                    }
                }
            }

            override fun afterTextChanged(t: Editable?) {
            }

        })


        // next button
        binding.btnSendOtp.setOnClickListener {
                if (isValidMobile(binding.etPhoneSA.text.toString())) {
                    val number = binding.etPhoneSA.text.toString()
                    val code = binding.countryCodePicker.selectedCountryCode
                    //   phone = "$code${Integer.parseInt(number)}"
                    // Log.d(TAG, phone)
                    try {
                        phone = "$code${Integer.parseInt(number)}"
                        viewModel.getLoginApiResponse(LoginEntity(token, phone))
                    } catch (ex: NumberFormatException) { // handle your exception
                        Log.d(TAG, ex.message.toString())
                        showToast(getString(R.string.phone_is_not_correct))
                    }
                } else
                    showToast(getString(R.string.enter_valid_phone))
        }
    }


    private fun initObserver() {
        viewModel.mState.flowWithLifecycle(
            this.lifecycle, Lifecycle.State.STARTED
        ).onEach {
            handleState(it)
        }.launchIn(this.lifecycleScope)
    }

    private fun isValidMobile(phone: String): Boolean {
        return android.util.Patterns.PHONE.matcher(phone).matches()
    }

    private fun handleState(state: LoginStateModel) {
        when (state) {
            is LoginStateModel.IsLoading -> handleIsLoadingState(state.isLoading)
            is LoginStateModel.LoginResponse -> handleApiResponse(state.loginApiResponse)
            is LoginStateModel.GenericError -> handleGenericError(state.message)
            is LoginStateModel.StatusFailed -> handleFailure(state.message)
            else -> {
            }
        }
    }

    private fun handleFailure(message: String) {
        if (message == "User not found") {
            lifecycleScope.launch {
                Utils.addStringValue(preferences, Constants.PHONE, phone)
            }
            goToOTPScreen(Constants.NEW_USER)
        } else
            showToast(message)

        binding.spinKit.visibility = View.GONE
    }


    private fun handleGenericError(message: String) {
        binding.spinKit.visibility = View.GONE
        Log.d(TAG, message)
        showToast(message)

    }

    private fun handleApiResponse(loginApiResponse: LoginApiResponse) {
        Log.d(TAG, loginApiResponse.first_name)
        saveData(loginApiResponse)
        goToOTPScreen(Constants.OLD_USER)

    }

    private fun handleIsLoadingState(loading: Boolean) {
        if (loading) {
            Log.d(TAG, "show loader....")
            binding.spinKit.visibility = View.VISIBLE
        } else {
            Log.d(TAG, "..... stop loader")
            binding.spinKit.visibility = View.GONE
        }
    }

    private fun goToOTPScreen(user: String) {
        val intent = Intent(this, OTPActivity::class.java)
        intent.putExtra(Constants.USER_TYPE, user)
        intent.putExtra("phoneCode", phone)
        startActivity(intent)
    }

    private fun generateFCMToken() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            token = task.result

            lifecycleScope.launch {
                Utils.addStringValue(preferences, Constants.FCM_TOKEN, token)
            }

            Log.d(TAG, token)

        })
    }

    private fun saveData(response: LoginApiResponse) {
        lifecycleScope.launch {
            Utils.addStringValue(preferences, Constants.FNAME, response.first_name)
            Utils.addStringValue(preferences, Constants.LNAME, response.last_name)
            Utils.addStringValue(preferences, Constants.EMAIL, response.email)
            Utils.addStringValue(preferences, Constants.REFERRAL_CODE, response.code)
            Utils.addStringValue(preferences, Constants.PHONE, phone)
        }
    }
}