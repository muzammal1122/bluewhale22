package com.app.bluewhale22.presentation.menu

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.domain.menu.entity.EmailEntity
import com.app.bluewhale22.domain.menu.entity.ProfileEntity
import com.app.bluewhale22.domain.menu.usecase.EmailUseCase
import com.app.bluewhale22.domain.menu.usecase.UpdateProfileApiUsecase
import com.app.bluewhale22.presentation.menu.profile.MenuModelState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

class MenuViewModel @Inject constructor(val usecase: UpdateProfileApiUsecase,val emailUseCase: EmailUseCase) :
    ViewModel() {

    private val state = MutableStateFlow<MenuModelState>(MenuModelState.Init)
    val mState: StateFlow<MenuModelState> get() = state
    private val TAG = "ProfileViewModel"
    private fun setLoading() {

        state.value = MenuModelState.IsLoading(true)
    }

    private fun hideLoading() {

        state.value = MenuModelState.IsLoading(false)
    }

    fun getUpdateProfileResponse(phone: String, entity: ProfileEntity) {
        viewModelScope.launch {
            usecase.execute(UpdateProfileApiUsecase.Params(phone, entity))
                .onStart {
                    Log.d(TAG, " Called on start")
                    setLoading()
                }
                .collect {
                    hideLoading()
                    Log.d(TAG, " Called collect")
                    when (it) {
                        is DataState.GenericError -> {
                            Log.d(TAG, " Called Generic error")
                        }

                        is DataState.Success -> {
                            Log.d(TAG, "Enter SUCCESS")
                            val status = it.value.response?.status ?: "Unknown"
                            if (status == "FAILED") {
                                val message = it.value.response?.message ?: "Unknown FAILED message"
                                Log.d(TAG, message)
                                state.value = MenuModelState.StatusFailed(message)
                            } else {
                                val message = it.value.data?.message ?: "Unknown"
                                Log.d(TAG, message)
                                state.value = it.value.data?.let { it1 ->
                                    MenuModelState.ProfileResponse(it1)
                                }!!
                            }

                        }
                    }
                }
        }

    }

    fun getEmailResponse(entity: EmailEntity) {
        viewModelScope.launch {
            emailUseCase.execute(EmailUseCase.Params(entity))
                .onStart {
                    setLoading()
                }
                .collect {
                    hideLoading()
                    Log.d(TAG, " Called collect")
                    when (it) {
                        is DataState.GenericError -> {
                            Log.d(TAG, " Called Generic error")
                        }

                        is DataState.Success -> {
                            Log.d(TAG, "Enter SUCCESS")
                            val status = it.value.response?.status ?: "Unknown"
                            if (status == "FAILED") {
                                val message = it.value.response?.message ?: "Unknown FAILED message"
                                Log.d(TAG, message)
                                state.value = MenuModelState.StatusFailed(message)
                            } else {

                                state.value = it.value.data?.let { it1 ->
                                    MenuModelState.GetEmailResponse(it1)
                                }!!
                            }

                        }
                    }
                }
        }
    }
}