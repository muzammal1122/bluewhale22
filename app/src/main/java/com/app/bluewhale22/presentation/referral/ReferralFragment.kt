package com.app.bluewhale22.presentation.referral

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.app.bluewhale22.R
import com.app.bluewhale22.databinding.ReferralFragmentBinding
import com.app.bluewhale22.domain.referral.entity.ReferralApiResponse
import com.app.bluewhale22.presentation.parent.ParentActivity.Companion.phone
import com.app.bluewhale22.utils.Constants
import com.app.bluewhale22.utils.Utils
import com.app.bluewhale22.utils.Utils.generateQRCode
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class ReferralFragment : Fragment() {

    @Inject
    lateinit var viewModel: ReferralViewModel

    @Inject
    lateinit var preferences: DataStore<Preferences>
    lateinit var binding: ReferralFragmentBinding
    private val TAG = "ReferralFragment"
    private var code: String = "no code"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.referral_fragment, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initObserver()

        lifecycleScope.launch {
            binding.responseR = ReferralApiResponse(
                Utils.getStringValue(preferences, Constants.CODE).first() ?: "--",
                Utils.getStringValue(preferences, Constants.USERS).first() ?: "--",
                Utils.getStringValue(preferences, Constants.FOLLOWERS).first() ?: "--"
            )
        }

        viewModel.getReferralData(phone)
        lifecycleScope.launch {
            val referralCode =
                Utils.getStringValue(preferences, Constants.REFERRAL_CODE).first() ?: ""
            binding.imgQrRF.setImageBitmap(generateQRCode(referralCode))
        }

        // go to notification screen
        binding.imgNotiRF.setOnClickListener {
            showToast(getString(R.string.no_notifications))
//            fragmentManager?.commit {
//                setReorderingAllowed(true)
//                add<NotificationsFragment>(R.id.fragment_container_view)
//            }
        }

        binding.imgShareRF.setOnClickListener {
            if (code == "no code") {
                showToast("Please wait")
            } else {
                val sendIntent: Intent = Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(Intent.EXTRA_TEXT, "Hi, please use my referral code to login:  https://www.bluewhale.host/ref.php?refcode=$code")
                    type = "text/plain"
                }

                val shareIntent = Intent.createChooser(sendIntent, null)
                startActivity(shareIntent)
            }
        }
    }


    private fun initObserver() {


        viewModel.mState.flowWithLifecycle(
            viewLifecycleOwner.lifecycle, Lifecycle.State.STARTED
        ).onEach {
            handleState(it)
            Log.d(TAG, it.javaClass.toString())
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun handleState(state: ReferralStateModel) {
        when (state) {
            is ReferralStateModel.IsLoading -> handleIsLoadingState(state.isLoading)
            is ReferralStateModel.Response -> handleApiResponse(state.response)
            is ReferralStateModel.GenericError -> handleGenericError(state.message)
            is ReferralStateModel.StatusFailed -> handleFailure(state.message)
            else -> {
            }
        }
    }

    private fun handleFailure(message: String) {
        showToast(message)
    }

    private fun handleGenericError(message: String) {
        Log.d(TAG, message)
        showToast(message)

    }


    private fun handleApiResponse(response: ReferralApiResponse) {
        Log.d(TAG, response.code)
        binding.responseR = response
        code = response.code


        //SAVE DATA
        lifecycleScope.launch {
            Utils.addStringValue(preferences, Constants.CODE, response.code)
            Utils.addStringValue(preferences, Constants.USERS, response.users)
            Utils.addStringValue(preferences, Constants.FOLLOWERS, response.followers)
        }

    }

    private fun handleIsLoadingState(loading: Boolean) {
        if (loading)
            Log.d(TAG, "show loader....")
        else
            Log.d(TAG, "..... stop loader")
    }

    private fun showToast(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }
}