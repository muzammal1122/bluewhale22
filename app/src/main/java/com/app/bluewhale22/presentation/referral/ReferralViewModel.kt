package com.app.bluewhale22.presentation.referral

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.domain.referral.entity.ReferralStatusEntity
import com.app.bluewhale22.domain.referral.usecase.ReferralApiUseCase
import com.app.bluewhale22.domain.referral.usecase.ReferralCodeUseCase
import com.app.bluewhale22.domain.referral.usecase.ReferralStatusUseCase
import com.app.bluewhale22.utils.Constants
import com.google.gson.Gson
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

class ReferralViewModel @Inject constructor(private val useCaseData: ReferralApiUseCase, private val useCaseCode: ReferralCodeUseCase, private val useCaseStatusUseCase: ReferralStatusUseCase) : ViewModel() {
    private val state = MutableStateFlow<ReferralStateModel>(ReferralStateModel.Init)
    val mState: StateFlow<ReferralStateModel> get() = state
    private val TAG = "ReferralViewModel"
    private fun setLoading() {
        state.value = ReferralStateModel.IsLoading(true)
    }

    private fun hideLoading() {

        state.value = ReferralStateModel.IsLoading(false)
    }

    fun getReferralData(phone: String) {
        viewModelScope.launch {
            useCaseData.execute(ReferralApiUseCase.Params(phone))
                .onStart {
                    setLoading()
                }
                .collect {
                    Log.d(TAG, " Called collect")
                    when (it) {
                        is DataState.GenericError -> {
                            Log.d(TAG, "Enter generic error")
                            val message =
                                it.error?.errorResponse?.errorMessage ?: Constants.UNKNOWN_ERROR
                            Log.d(TAG, "Message: $message")
                            state.value = ReferralStateModel.GenericError(message)

                        }

                        is DataState.Success -> {
                            Log.d(TAG, "Enter SUCCESS")
                            val status = it.value.response?.status ?: "Unknown"
                            if (status == "FAILED") {
                                val message = it.value.response?.message ?: "Unknown FAILED message"
                                Log.d(TAG, message)
                                state.value = ReferralStateModel.StatusFailed(message)
                            } else {
                                Log.d(TAG, Gson().toJson(it.value))
                                setLoading()
                                it.value.data?.let {
                                    Log.d(TAG, Gson().toJson(it))
                                    state.value = ReferralStateModel.Response(it)

                                }
                            }

                        }
                    }

                }
        }
    }

    fun getReferralCode(){
        viewModelScope.launch {
           useCaseCode.execute(ReferralCodeUseCase.Params())
               .onStart {
                   setLoading()
               }
               .collect {
                   hideLoading()
                   Log.d(TAG, " Called collect")
                   when (it) {
                       is DataState.GenericError -> {
                           Log.d(TAG, "Enter generic error")
                           val message =
                               it.error?.errorResponse?.errorMessage ?: Constants.UNKNOWN_ERROR
                           Log.d(TAG, "Message: $message")
                           state.value = ReferralStateModel.GenericError(message)

                       }

                       is DataState.Success -> {
                           Log.d(TAG, "Enter SUCCESS")
                           val status = it.value.response?.status ?: "Unknown"
                           if (status == "FAILED") {
                               val message = it.value.response?.message ?: "Unknown FAILED message"
                               Log.d(TAG, message)
                               state.value = ReferralStateModel.StatusFailed(message)
                           } else {
                               Log.d(TAG, Gson().toJson(it.value))
                               setLoading()
                               it.value.data?.let {
                                   Log.d(TAG, Gson().toJson(it))
                                   state.value = ReferralStateModel.CodeResponse(it)

                               }
                           }

                       }
                   }
               }
        }
    }

    fun getReferralStatus(entity: ReferralStatusEntity){
        viewModelScope.launch {
            useCaseStatusUseCase.execute(ReferralStatusUseCase.Params(entity))
                .onStart {
                    setLoading()
                }
                .collect {
                    hideLoading()
                    Log.d(TAG, " Called collect")
                    when (it) {
                        is DataState.GenericError -> {
                            Log.d(TAG, "Enter generic error")
                            val message =
                                it.error?.errorResponse?.errorMessage ?: Constants.UNKNOWN_ERROR
                            Log.d(TAG, "Message: $message")
                            state.value = ReferralStateModel.GenericError(message)

                        }

                        is DataState.Success -> {
                            Log.d(TAG, "Enter SUCCESS")
                            val status = it.value.response?.status ?: "Unknown"
                            if (status == "FAILED") {
                                val message = it.value.response?.message ?: "Unknown FAILED message"
                                Log.d(TAG, message)
                                state.value = ReferralStateModel.StatusFailed(message)
                            } else {
                                Log.d(TAG, Gson().toJson(it.value))
                                setLoading()
                                it.value.data?.let {
                                    Log.d(TAG, Gson().toJson(it))
                                    state.value = ReferralStateModel.StatusResponse(it)

                                }
                            }

                        }
                    }
                }
        }
    }
}