package com.app.bluewhale22.presentation.home

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.app.bluewhale22.R
import com.app.bluewhale22.databinding.HomeFragmentBinding
import com.app.bluewhale22.domain.home.entity.AddMoreEntity
import com.app.bluewhale22.domain.home.entity.AddMoreResponse
import com.app.bluewhale22.domain.home.entity.HomeApiResponse
import com.app.bluewhale22.presentation.fcm.MyFirebaseMessagingService
import com.app.bluewhale22.presentation.fcm.OnStatsChanged
import com.app.bluewhale22.presentation.login.PhoneNumberActivity
import com.app.bluewhale22.presentation.menu.contactus.ContactUsActivity
import com.app.bluewhale22.presentation.menu.profile.ProfileFragment
import com.app.bluewhale22.presentation.parent.ParentActivity.Companion.phone
import com.app.bluewhale22.utils.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : Fragment(), ProgressDialogListener, WinDialogListener {

    @Inject
    lateinit var viewModel: HomeViewModel

    @Inject
    lateinit var preferences: DataStore<Preferences>
    private val TAG = "HomeFragment"
    lateinit var binding: HomeFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.home_fragment, container, false)
        return binding.root
    }


    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObserver()

        //load saved values from Room DB
        lifecycleScope.launch {
            binding.response = HomeApiResponse(
                Utils.getStringValue(preferences, Constants.TOTAL_BALANCE).first() ?: "--",
                Utils.getStringValue(preferences, Constants.USDT).first() ?: "--",
                Utils.getStringValue(preferences, Constants.USDT_VALUE).first() ?: "--",
                Utils.getStringValue(preferences, Constants.PREBOOKING).first() ?: "--",
                Utils.getStringValue(preferences, Constants.MARKET_CAP).first() ?: "--",
                Utils.getStringValue(preferences, Constants.PERCENTAGE).first() ?: "--",
                Utils.getBooleanValue(preferences, Constants.ARROW_POSITION).first() ?: false,
                ""
            )
            // show progress
            val a = (Utils.getStringValue(preferences, Constants.PRE_AMOUNT).first() ?: "0")
            binding.circularProgressBar.progress = a.toFloat()
            binding.txtPercentageHome.text = "${a.toInt()}%"
        }

        //load saved values from Data Store
        lifecycleScope.launch {
            binding.txtFullName.text = "${
                Utils.getStringValue(preferences, Constants.FNAME).first() ?: "--"
            } ${Utils.getStringValue(preferences, Constants.LNAME).first() ?: "--"}"

        }

        // load home data
        viewModel.getHomeData(phone)

        // listen change from server and refresh home data
        lifecycleScope.launch {
            MyFirebaseMessagingService.state.collect {
                when (it) {
                    is OnStatsChanged -> {
                        handleStatsChanged()
                    }
                }
            }
        }

        // refresh page
        binding.swiperefresh.setOnRefreshListener {
            binding.swiperefresh.isRefreshing = true
            viewModel.getHomeData(phone)
        }

        // add more
        binding.rlAddHF.setOnClickListener {
            try {
                val bookingAmount = binding.etAddMoreHF.text.toString()
                if (bookingAmount.isNotEmpty()) {
                    if (bookingAmount.toInt() in 1..1000) {
                        hideKeyBoard()
                        binding.etAddMoreHF.isCursorVisible = false
                        Log.d(TAG, "phone...   ${binding.etAddMoreHF.text}")
                        viewModel.addMore(
                            AddMoreEntity(
                                view.findViewById<EditText>(R.id.etAddMoreHF).text.toString()
                                    .toInt(), phone
                            )
                        )
                        binding.etAddMoreHF.text.clear()
                    } else {
                        showToast(getString(R.string.enter_bw_1_100))
                    }
                }
            } catch (e: Exception) {
                showToast(getString(R.string.invalid_input))
            }

        }

        // go to profile screen
        binding.imgProfileHome.setOnClickListener {
            val frag = ProfileFragment()
            frag.parent = "Home"
            fragmentManager?.commit {
                setReorderingAllowed(true)
                replace(R.id.fragment_container_view, frag)
            }
        }

        // go to notification screen
        binding.imgNotificationHome.setOnClickListener {
            showToast(getString(R.string.no_notifications))

        }

        // go to contactus screen
        binding.txtContactHF.setOnClickListener {
            startActivity(Intent(activity, ContactUsActivity::class.java))
        }

        //show info dialog
        binding.imgQuestionTHF.setOnClickListener {
            ShowInfoDialog(requireContext()).showPopup()
        }
    }

    private fun handleStatsChanged() {
        viewModel.getHomeData(phone)
    }

    private fun initObserver() {
        viewModel.mState.flowWithLifecycle(
            this.lifecycle, Lifecycle.State.STARTED
        ).onEach {
            handleState(it)
        }.launchIn(this.lifecycleScope)
    }

    private fun handleState(state: HomeStateModel) {
        when (state) {
            is HomeStateModel.IsLoading -> handleIsLoadingState(state.isLoading)
            is HomeStateModel.Response -> handleApiResponse(state.response)
            is HomeStateModel.ResponseAdd -> handleAddMoreApiResponse(state.response)
            is HomeStateModel.GenericError -> handleGenericError(state.message)
            is HomeStateModel.StatusFailed -> handleFailure(state.message)
            else -> {
            }
        }
    }

    private fun handleAddMoreApiResponse(response: AddMoreResponse) {
        Log.d(TAG, response.total_pre_booked_amount + "      pre amount")
        binding.response = HomeApiResponse(
            response.total_balance,
            response.usdt,
            response.usdt_value,
            response.preBooking,
            response.market_cap,
            response.percentage,
            response.arrowPosition,
            response.total_pre_booked_amount
        )

        // update values
        lifecycleScope.launch {
            Utils.addStringValue(preferences, Constants.TOTAL_BALANCE, response.total_balance)
            Utils.addStringValue(preferences, Constants.USDT, response.usdt)
            Utils.addStringValue(preferences, Constants.USDT_VALUE, response.usdt_value)
            Utils.addStringValue(preferences, Constants.PREBOOKING, response.preBooking)
            Utils.addStringValue(preferences, Constants.MARKET_CAP, response.market_cap)
            Utils.addStringValue(preferences, Constants.PERCENTAGE, response.percentage)
            Utils.addStringValue(preferences, Constants.PRE_AMOUNT, response.total_pre_booked_amount.toInt() .div(1000) .times(100).toString())
            Utils.addBooleanValue(preferences, Constants.ARROW_POSITION, response.arrowPosition)
        }

        // progress bar
        val p = (response.total_pre_booked_amount.toDouble() .div(1000)) * 100
        binding.circularProgressBar.progress = p.toFloat()

        binding.txtPercentageHome.text = "${p.toInt()}%"
    }

    private fun handleFailure(message: String) {
        if (message == "User not found") {
            val intent = Intent(activity, PhoneNumberActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        } else
            showToast(message)
    }

    private fun handleGenericError(message: String) {
        Log.d(TAG, message)
        showToast(message)

    }

    private fun showToast(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    private fun handleApiResponse(response: HomeApiResponse) {
        Log.d(TAG, response.market_cap + "    this  pre amount")
        binding.response = response

        // save values
        saveData(response)

        lifecycleScope.launch {
            val a = Utils.getBooleanValue(preferences, "dont_show").first() ?: false
            val b = Utils.getBooleanValue(preferences, "winDialog").first() ?: false

            Log.d(TAG, "  b   $b")
            Log.d(TAG, "a    $a")

            if (!b) {
                context?.let {
                    ShowWinDialog(
                        it,
                        response.total_balance,
                        this@HomeFragment
                    ).showPopup()
                }
            } else {

                if (!a) {
                    context?.let { ShowProgress(it, this@HomeFragment).showPopup() }
                }
            }
        }

        if (response.arrowPosition) {
            binding.imgArrow.setImageResource(R.drawable.ic_baseline_arrow_drop_up_24)
            binding.txtPercentageHF.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.green
                )
            )
        } else {
            binding.imgArrow.setImageResource(R.drawable.ic_baseline_arrow_drop_down_24)
            binding.txtPercentageHF.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.red
                )
            )
        }
        binding.swiperefresh.isRefreshing = false

        // progress bar
        val p = (response.total_pre_booked_amount.toDouble() .div(1000)) * 100
        binding.circularProgressBar.progress = p.toFloat()

        binding.txtPercentageHome.text = "${p.toInt()}%"

    }

    private fun saveData(response: HomeApiResponse) {
        lifecycleScope.launch {
            Utils.addStringValue(preferences, Constants.TOTAL_BALANCE, response.total_balance)
            Utils.addStringValue(preferences, Constants.USDT, response.usdt)
            Utils.addStringValue(preferences, Constants.USDT_VALUE, response.usdt_value)
            Utils.addStringValue(preferences, Constants.PREBOOKING, response.preBooking)
            Utils.addStringValue(preferences, Constants.MARKET_CAP, response.market_cap)
            Utils.addStringValue(preferences, Constants.PERCENTAGE, response.percentage)
            Utils.addStringValue(preferences, Constants.PRE_AMOUNT, response.total_pre_booked_amount.toInt() .div(1000) .times(100).toString())
            Utils.addBooleanValue(preferences, Constants.ARROW_POSITION, response.arrowPosition)
        }
    }

    private fun handleIsLoadingState(loading: Boolean) {
        if (loading)
            Log.d(TAG, "show loader....")
        else
            Log.d(TAG, "..... stop loader")
    }

    private fun hideKeyBoard() {
        val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(requireView().windowToken, 0)
    }

    override fun onDontShowCheck() {
        lifecycleScope.launch {
            Utils.addBooleanValue(preferences, "dont_show", true)
        }
    }

    override fun onDialogHide() {
        lifecycleScope.launch {
            Utils.addBooleanValue(preferences, "winDialog", true)
        }
        context?.let { ShowProgress(it, this@HomeFragment).showPopup() }
    }


}