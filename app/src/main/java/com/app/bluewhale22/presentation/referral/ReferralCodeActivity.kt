package com.app.bluewhale22.presentation.referral

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.app.bluewhale22.BaseActivity
import com.app.bluewhale22.R
import com.app.bluewhale22.domain.referral.entity.ReferralCodeResponse
import com.app.bluewhale22.domain.referral.entity.ReferralStatusEntity
import com.app.bluewhale22.domain.referral.entity.ReferralStatusResponse
import com.app.bluewhale22.presentation.password.PasswordActivity
import com.app.bluewhale22.utils.Constants
import com.app.bluewhale22.utils.showToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
class ReferralCodeActivity : BaseActivity() {
    @Inject
    lateinit var viewModel: ReferralViewModel
    lateinit var binding: com.app.bluewhale22.databinding.ActivityReferralCodeBinding
    private var phone: String = ""
    private val TAG = "ReferralCodeActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_referral_code)
        initObserver()

        // receive data from previous activity
        phone = intent.extras?.getString("phoneCode") ?: "123456789"

        binding.imgBackCPA.setOnClickListener {
            onBackPressed()
        }

        binding.btnRequest.setOnClickListener {
            viewModel.getReferralCode()
        }


        binding.btnNextFR.setOnClickListener {
            if (!binding.etReferralFR.text.isNullOrEmpty()) {
                if (binding.etReferralFR.text.length == 8) {
                    if (phone == "123456789"){
                       showToast(getString(R.string.phone_is_missing))
                    }
                    viewModel.getReferralStatus(
                        ReferralStatusEntity(
                            binding.etReferralFR.text.toString(),
                            phone
                        )
                    )
                } else
                    showToast(getString(R.string.enter_valid_code))
            }else
                showToast(getString(R.string.enter_code))
        }
    }

    private fun initObserver() {
        viewModel.mState.flowWithLifecycle(
            this.lifecycle, Lifecycle.State.STARTED
        ).onEach {
            handleState(it)
        }.launchIn(this.lifecycleScope)
    }

    private fun handleState(state: ReferralStateModel) {
        when (state) {
            is ReferralStateModel.IsLoading -> handleIsLoadingState(state.isLoading)
            is ReferralStateModel.CodeResponse -> handleApiResponse(state.response)
            is ReferralStateModel.StatusResponse -> handleStatusResponse(state.response)
            is ReferralStateModel.GenericError -> handleGenericError(state.message)
            is ReferralStateModel.StatusFailed -> handleFailure(state.message)
            else -> {
            }
        }
    }

    private fun handleStatusResponse(response: ReferralStatusResponse) {
        Log.d(TAG, "status success....  $response.status")
        showToast(response.status)
        val intent = Intent(this, PasswordActivity::class.java)
        intent.putExtra(Constants.USER_TYPE, Constants.NEW_USER)
        startActivity(intent)
    }

    private fun handleFailure(message: String) {
        Log.d(TAG, "fail....  $message")
        binding.spinKit.visibility = GONE
        showToast(message)
    }

    private fun handleGenericError(message: String) {
        Log.d(TAG, "error....  $message")
        binding.spinKit.visibility = GONE
        showToast(message)

    }


    private fun handleApiResponse(response: ReferralCodeResponse) {
        Log.d(TAG, "success....  " + response.code)
        binding.response = response
        binding.spinKit.visibility = GONE
    }

    private fun handleIsLoadingState(loading: Boolean) {
        if (loading) {
            Log.d(TAG, "show loader....")
            binding.spinKit.visibility = VISIBLE
        } else {
            Log.d(TAG, "..... stop loader")
            binding.spinKit.visibility = GONE
        }
    }

}