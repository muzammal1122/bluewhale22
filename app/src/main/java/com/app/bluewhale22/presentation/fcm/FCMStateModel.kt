package com.app.bluewhale22.presentation.fcm

import com.google.firebase.messaging.FcmBroadcastProcessor

sealed class FCMStateModel
object OnStatsChanged:FCMStateModel()
object Init:FCMStateModel()
data class OnDataReceive(var data:String): FCMStateModel()
