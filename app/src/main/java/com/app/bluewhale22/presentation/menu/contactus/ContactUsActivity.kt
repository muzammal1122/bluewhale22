package com.app.bluewhale22.presentation.menu.contactus

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.app.bluewhale22.BaseActivity
import com.app.bluewhale22.R
import com.app.bluewhale22.databinding.ActivityContactUsBinding
import com.app.bluewhale22.domain.menu.entity.EmailEntity
import com.app.bluewhale22.domain.menu.entity.EmailResponse
import com.app.bluewhale22.presentation.menu.MenuViewModel
import com.app.bluewhale22.presentation.menu.profile.MenuModelState
import com.app.bluewhale22.utils.showToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
class ContactUsActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: MenuViewModel
    lateinit var binding: ActivityContactUsBinding
    private var emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
    val TAG: String = "ContactUsActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_contact_us)


        initObserver()

        binding.btnSendCUA.setOnClickListener {
            when {
                binding.etEmailCUA.text.isNullOrEmpty() -> {
                    showToast(getString(R.string.enter_email))
                }
                binding.etSubjectCUA.text.isNullOrEmpty() -> {
                    showToast(getString(R.string.enter_message))
                }
                !binding.etEmailCUA.text.trim().matches(emailPattern.toRegex()) -> {
                    showToast(getString(R.string.enter_valid_email))
                }
                else -> {
                    viewModel.getEmailResponse(
                        EmailEntity(
                            binding.etEmailCUA.text.toString(),
                            binding.etSubjectCUA.text.toString()
                        )
                    )
                }
            }
        }

        binding.imgBackCUA.setOnClickListener {
            onBackPressed()
        }

        binding.etSubjectCUA.addTextChangedListener(getTextWatcher())
    }


    private fun getTextWatcher():TextWatcher{
        return object:TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(text: Editable?) {
                binding.tvCounterCA.text = "${text.toString().length}/300"
            }

        }
    }

    private fun initObserver() {
        viewModel.mState.flowWithLifecycle(
            this.lifecycle, Lifecycle.State.STARTED
        ).onEach {
            handleState(it)
        }.launchIn(this.lifecycleScope)
    }

    private fun handleState(state: MenuModelState) {
        when (state) {
            is MenuModelState.IsLoading -> handleIsLoadingState(state.isLoading)
            is MenuModelState.GetEmailResponse -> handleApiResponse(state.emailResponse)
            is MenuModelState.FoundException -> handleException(state.exception)
            is MenuModelState.StatusFailed -> handleFailure(state.message)
            else -> {
            }
        }
    }

    private fun handleException(exception: Exception) {
        Log.d(TAG, exception.message.toString())
        binding.spinKit.visibility = View.GONE
    }

    private fun handleApiResponse(response: EmailResponse) {
        Log.d(TAG, "SUCCESS   $response.message")
        binding.spinKit.visibility = View.GONE
        showToast(response.msg)
    }

    private fun handleIsLoadingState(loading: Boolean) {
        if (loading) {
            Log.d(TAG, "show loader....")
            binding.spinKit.visibility = View.VISIBLE
        } else {
            Log.d(TAG, "..... stop loader")
            binding.spinKit.visibility = View.GONE
        }
    }

    private fun handleFailure(message: String) {
        Log.d(TAG, "failure    $message")
        binding.spinKit.visibility = View.GONE
        showToast(message)
    }

}
