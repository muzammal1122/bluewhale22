package com.app.bluewhale22.presentation.menu.profile

import com.app.bluewhale22.domain.menu.entity.EmailResponse
import com.app.bluewhale22.domain.menu.entity.UpdateProfileResponse

sealed class MenuModelState {
    object Init : MenuModelState()
    data class IsLoading(val isLoading: Boolean) : MenuModelState()
    data class ProfileResponse(val profileResponse: UpdateProfileResponse) : MenuModelState()
    data class GetEmailResponse(val emailResponse: EmailResponse) : MenuModelState()
    data class FoundException(val exception: Exception) : MenuModelState()
    data class StatusFailed(val message: String) : MenuModelState()
}