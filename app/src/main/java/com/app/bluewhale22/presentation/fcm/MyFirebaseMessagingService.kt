package com.app.bluewhale22.presentation.fcm

import android.util.Log
import com.app.bluewhale22.utils.Constants
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class MyFirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        Log.d(TAG, "From: ${remoteMessage.from}")

        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {

            val data = remoteMessage.data
            val action = data["action"]
            if(action.equals(Constants.STATS_CHANGED)){
                GlobalScope.launch {
                    state.emit(OnStatsChanged)
                }
            }



            Log.d(TAG, "Message data payload: ${remoteMessage.data}")


        }

        // Check if message contains a notification payload.
        remoteMessage.notification?.let {
            Log.d(TAG, "Message Notification Body: ${it.body}")
        }
    }
    override fun onNewToken(token: String) {
        Log.d(TAG, "Refreshed token: $token")
    }
    companion object {

        private const val TAG = "MyFirebaseMsgService"
        val state = MutableStateFlow<FCMStateModel>(Init)

    }
}