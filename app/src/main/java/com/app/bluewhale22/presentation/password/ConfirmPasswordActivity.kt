package com.app.bluewhale22.presentation.password

import `in`.aabhasjindal.otptextview.OTPListener
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import com.app.bluewhale22.BaseActivity
import com.app.bluewhale22.R
import com.app.bluewhale22.databinding.ActivityConfirmPasswordBinding
import com.app.bluewhale22.presentation.parent.ParentActivity
import com.app.bluewhale22.presentation.signup.WelcomeActivity
import com.app.bluewhale22.utils.Constants
import com.app.bluewhale22.utils.Utils
import com.app.bluewhale22.utils.gone
import com.app.bluewhale22.utils.visible
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ConfirmPasswordActivity : BaseActivity() {

    private lateinit var code: String
    lateinit var binding: ActivityConfirmPasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_confirm_password)

        code = intent.extras?.getString("loginCode") ?: "noCode"

        binding.imgBackCPA.setOnClickListener {
            onBackPressed()
        }


        binding.otpView.otpListener  = object : OTPListener {
            override fun onInteractionListener() {

            }

            override fun onOTPComplete(otp: String) {
                if (otp == code) {
                    hideKeyBoard()
                    // save password in preferences
                    lifecycleScope.launch {
                        Utils.addStringValue(
                            preferences,
                            Constants.Login_KEY,
                            otp
                        )
                    }
                    // move to home
                   moveToHome()
                } else {
                    binding.txtInvalidCodeCPA.visible()
                    binding.txtInfoCPA.gone()
                    binding.otpView.showError()
                }
            }
        }


    }

    private fun moveToHome(){
        val intent = Intent(this, WelcomeActivity::class.java)
        startActivity(intent)
    }

    private fun hideKeyBoard() {
        val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(View(this).windowToken, 0)
    }
}