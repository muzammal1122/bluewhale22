package com.app.bluewhale22.presentation.signup

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.domain.signUp.entity.SignUpEntity
import com.app.bluewhale22.domain.signUp.usesase.SignUpApiUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

class SignUpViewModel @Inject constructor(private val signUpApiUseCase: SignUpApiUseCase) :
    ViewModel() {

    private val state = MutableStateFlow<SignupStateModel>(SignupStateModel.Init)
    val mState: StateFlow<SignupStateModel> get() = state
    private val TAG = "SignUpViewModel"
    private fun setLoading() {

        state.value = SignupStateModel.IsLoading(true)
    }

    private fun hideLoading() {

        state.value = SignupStateModel.IsLoading(false)
    }


    fun getSignUpApiResponse(entity: SignUpEntity) = viewModelScope.launch {
        signUpApiUseCase.execute(SignUpApiUseCase.Params(entity))
            .onStart {
                Log.d(TAG, " Called on start")
                setLoading()
            }
            .collect {
                hideLoading()
                Log.d(TAG, " Called collect")
                when (it) {
                    is DataState.GenericError -> {
                        Log.d(TAG, " Called Generic error")
                    }

                    is DataState.Success -> {
                        Log.d(TAG, "Enter SUCCESS")
                        val status = it.value.response?.status ?: "Unknown"
                        if (status == "FAILED") {
                            val message = it.value.response?.message ?: "Unknown FAILED message"
                            Log.d(TAG, message)
                            state.value = SignupStateModel.StatusFailed(message)
                        } else {
                            val message = it.value.data?.message ?: "Unknown"
                            Log.d(TAG, message)
                            state.value = it.value.data?.let { it1 ->
                                SignupStateModel.SignUpResponse(it1)
                            }!!
                        }

                    }
                }
            }

    }
}