package com.app.bluewhale22.presentation.news

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.fragment.app.Fragment
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.lifecycle.lifecycleScope
import com.app.bluewhale22.R
import com.app.bluewhale22.databinding.NewsFragmentBinding
import com.app.bluewhale22.presentation.notification.NotificationsFragment
import com.app.bluewhale22.utils.Constants
import com.app.bluewhale22.utils.Utils
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class NewsFragment : Fragment() {


    @Inject
    lateinit var preferences: DataStore<Preferences>
    lateinit var binding: NewsFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.news_fragment, container, false)
        return binding.root
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lifecycleScope.launch {
            binding.txtName.text =
                Utils.getStringValue(preferences, Constants.FNAME).first() ?: "--"
            binding.txtEmail.text =
                Utils.getStringValue(preferences, Constants.EMAIL).first() ?: "--"

        }

        //go to notification screen
        binding.imgNotificationNews.setOnClickListener {
            fragmentManager?.commit {
                setReorderingAllowed(true)
                add<NotificationsFragment>(R.id.fragment_container_view)
            }
        }

        binding.webView.settings.javaScriptEnabled = true

        binding.webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {
                binding.spinKit.visibility = GONE
            }

        }
        binding.webView.loadUrl("https://www.investing.com/news/cryptocurrency-news")
    }


}