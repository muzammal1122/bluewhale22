package com.app.bluewhale22.presentation.splash

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import com.app.bluewhale22.BaseActivity
import com.app.bluewhale22.R
import com.app.bluewhale22.databinding.ActivitySplashBinding
import com.app.bluewhale22.presentation.intro.IntroActivity
import com.app.bluewhale22.presentation.login.PhoneNumberActivity
import com.app.bluewhale22.presentation.password.PasswordActivity
import com.app.bluewhale22.utils.Constants
import com.app.bluewhale22.utils.Utils
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch

@SuppressLint("CustomSplashScreen")
@AndroidEntryPoint
class SplashActivity : BaseActivity() {


    lateinit var binding: ActivitySplashBinding
    private var loginStatus: String = "firstTime"
    private var loginCode: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)

        lifecycleScope.launch {
            loginStatus =
                Utils.getStringValue(preferences, Constants.Login_STATUS).first() ?: "firstTime"
            loginCode = Utils.getStringValue(preferences, Constants.Login_KEY).first() ?: "no code"
        }

        val sideAnimation = AnimationUtils.loadAnimation(this, R.anim.top_animation)
        val rightSideAnimation = AnimationUtils.loadAnimation(this, R.anim.right_slide)


        binding.ivLogoSA.startAnimation(sideAnimation)
        binding.txtLogoName.startAnimation(rightSideAnimation)

        CoroutineScope(Dispatchers.IO).launch {
            delaySplash()
        }
    }

    private suspend fun delaySplash() {
        val intent: Intent
        delay(3000)
       // Log.d("SplashActivity ", loginStatus)
        if (loginStatus == "firstTime") {
            intent = Intent(this, IntroActivity::class.java)
            lifecycleScope.launch {
                Utils.addStringValue(preferences, Constants.Login_STATUS, "already")
            }
        } else {
            if (loginCode == "no code") {
                intent = Intent(this, PhoneNumberActivity::class.java)
                intent.putExtra(Constants.USER_TYPE, Constants.NEW_USER)
            } else {
                intent = Intent(this, PasswordActivity::class.java)
                intent.putExtra(Constants.USER_TYPE, Constants.OLD_USER)
            }

        }
        startActivity(intent)
        finish()
    }
}