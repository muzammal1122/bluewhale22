package com.app.bluewhale22.presentation.signup

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.lifecycleScope
import com.app.bluewhale22.R
import com.app.bluewhale22.databinding.ActivityWelcomeBinding
import com.app.bluewhale22.presentation.parent.ParentActivity
import com.app.bluewhale22.utils.Constants
import com.app.bluewhale22.utils.Utils
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class WelcomeActivity : AppCompatActivity() {

    @Inject
    lateinit var preferences: DataStore<Preferences>
    lateinit var binding: ActivityWelcomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_welcome)


        lifecycleScope.launch {
            binding.txtUserNameWelcome.text =
                Utils.getStringValue(preferences, Constants.FNAME).first() ?: "--"
        }

        binding.btnNextFR.setOnClickListener {
            val intent = Intent(this, ParentActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }

    }
}