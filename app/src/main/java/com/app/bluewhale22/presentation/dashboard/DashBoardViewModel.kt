package com.app.bluewhale22.presentation.dashboard

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.domain.dashboard.usecase.DashBoardApiUseCase
import com.app.bluewhale22.utils.Constants
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

class DashBoardViewModel @Inject constructor(private val useCase: DashBoardApiUseCase) :
    ViewModel() {
    private val state = MutableStateFlow<DashBoardStateModel>(DashBoardStateModel.Init)
    val mState: StateFlow<DashBoardStateModel> get() = state
    private val TAG = "DashBoardViewModel"
    private fun setLoading() {

        state.value = DashBoardStateModel.IsLoading(true)
    }

    private fun hideLoading() {
        Log.d(TAG, " Called on start")
        state.value = DashBoardStateModel.IsLoading(false)
    }

    fun getDashBoardData() {
        viewModelScope.launch {
            useCase.execute(DashBoardApiUseCase.Params())
                .onStart {

                    setLoading()
                }
                .collect {
                    hideLoading()
                    Log.d(TAG, " Called collect")
                    when (it) {
                        is DataState.GenericError -> {
                            Log.d(TAG, "Enter generic error")
                            val message =
                                it.error?.errorResponse?.errorMessage ?: Constants.UNKNOWN_ERROR
                            Log.d(TAG, "Message: $message")
                            state.value = DashBoardStateModel.GenericError(message)

                        }

                        is DataState.Success -> {
                            Log.d(TAG, "Enter SUCCESS")
                            val status = it.value.response?.status ?: "Unknown"
                            if (status == "FAILED") {
                                val message = it.value.response?.message ?: "Unknown FAILED message"
                                Log.d(TAG, message)
                                state.value = DashBoardStateModel.StatusFailed(message)
                            } else {
                                val message = it.value.data?.market_price.toString() ?: "Unknown"
                                Log.d(TAG, message)
                                state.value = it.value.data?.let { it1 ->
                                    DashBoardStateModel.Response(it1)
                                }!!
                            }

                        }
                    }
                }
        }
    }
}