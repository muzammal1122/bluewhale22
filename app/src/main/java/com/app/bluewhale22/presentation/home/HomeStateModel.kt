package com.app.bluewhale22.presentation.home

import com.app.bluewhale22.domain.home.entity.AddMoreResponse
import com.app.bluewhale22.domain.home.entity.HomeApiResponse


sealed class HomeStateModel {
    object Init : HomeStateModel()
    data class IsLoading(val isLoading: Boolean) : HomeStateModel()
    data class Response(val response: HomeApiResponse) : HomeStateModel()
    data class ResponseAdd(val response: AddMoreResponse) : HomeStateModel()
    data class FoundException(val exception: Exception) : HomeStateModel()
    data class GenericError(val message: String) : HomeStateModel()
    data class StatusFailed(val message: String) : HomeStateModel()
}
