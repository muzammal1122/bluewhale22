package com.app.bluewhale22.presentation.login

import `in`.aabhasjindal.otptextview.OTPListener
import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import androidx.databinding.DataBindingUtil
import com.app.bluewhale22.BaseActivity
import com.app.bluewhale22.R
import com.app.bluewhale22.databinding.ActivityOtpactivityBinding
import com.app.bluewhale22.presentation.password.PasswordActivity
import com.app.bluewhale22.presentation.signup.SignUpActivity
import com.app.bluewhale22.utils.Constants
import com.app.bluewhale22.utils.gone
import com.app.bluewhale22.utils.showToast
import com.app.bluewhale22.utils.visible
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.*
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import java.util.concurrent.TimeUnit

class OTPActivity : BaseActivity() {

    private lateinit var auth: FirebaseAuth
    private val TAG = "OTPActivity"
    lateinit var binding: ActivityOtpactivityBinding

    private var storedVerificationId: String = ""
    private lateinit var resendToken: PhoneAuthProvider.ForceResendingToken
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    private var enterOTP: Boolean = true
    private var phone: String = ""
    private lateinit var userType: String

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_otpactivity)
        userType = intent.extras?.getString(Constants.USER_TYPE) ?: Constants.NEW_USER
        //initialize
        auth = Firebase.auth

        // receive data from previous activity
        phone = intent.extras?.getString("phoneCode")!!
        binding.txtEnterOTP.text = " +$phone"

        //when otp is done
        binding.otpViewO.otpListener = object : OTPListener {
            override fun onInteractionListener() {

            }

            override fun onOTPComplete(otp: String) {
                Log.d(TAG, " onOTPComplete   $otp")
                if (enterOTP) {
                    val credential: PhoneAuthCredential = PhoneAuthProvider.getCredential(
                        storedVerificationId, otp
                    )
                    signInWithPhoneAuthCredential(credential)
                }
            }
        }


        // count down timer
        startTimer()


        // START OTP FUNCTIONALITY
        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                enterOTP = false
                Log.d(TAG, "onVerificationCompleted:  sms :   ${credential.smsCode.toString()}")
                binding.otpViewO.setOTP(credential.smsCode.toString())
                signInWithPhoneAuthCredential(credential)
            }

            override fun onVerificationFailed(e: FirebaseException) {
                Log.d(TAG, "onVerificationFailed", e)

                if (e is FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    showToast("Invalid Request")
                } else if (e is FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    showToast("SMS Quota end")
                }
            }

            override fun onCodeSent(
                verificationId: String,
                token: PhoneAuthProvider.ForceResendingToken
            ) {
                // Save verification ID and resending token so we can use them later
                storedVerificationId = verificationId
                resendToken = token
            }

        }


        startPhoneNumberVerification("+$phone")

        // on resend button click
        binding.txtResendCodeOTP.setOnClickListener {
            if (::resendToken.isInitialized) {
                binding.txtInvalidCode.gone()
                resendVerificationCode("+$phone", resendToken)
                binding.txtResendCodeOTP.gone()
                binding.llTimerOTP.visible()
            } else {
                startPhoneNumberVerification("+$phone")
            }
        }
    }

    // START sign_in_with_phone
    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success")

                    val intent: Intent
                    if (userType == Constants.NEW_USER) {
                        intent = Intent(this, SignUpActivity::class.java)
                        intent.putExtra("phoneCode", phone)
                    } else {
                        intent = Intent(this, PasswordActivity::class.java)
                        intent.putExtra(Constants.USER_TYPE, Constants.NEW_USER)
                    }

                    startActivity(intent)

                } else {
                    // Sign in failed, display a message and update the UI
                    Log.d(TAG, "signInWithCredential:failure", task.exception)
                    if (task.exception is FirebaseAuthInvalidCredentialsException) {
                        // The verification code entered was invalid
                        binding.txtInvalidCode.visible()
                        binding.otpViewO.showError()
                    }
                }
            }
    }

    // START start_phone_auth
    private fun startPhoneNumberVerification(phoneNumber: String) {

        val options = PhoneAuthOptions.newBuilder(auth)
            .setPhoneNumber(phoneNumber)       // Phone number to verify
            .setTimeout(55L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(this)                 // Activity (for callback binding)
            .setCallbacks(callbacks)          // OnVerificationStateChangedCallbacks
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)

    }

    // [START resend_verification]
    private fun resendVerificationCode(

        phoneNumber: String,
        token: PhoneAuthProvider.ForceResendingToken?
    ) {
        // count down timer
        startTimer()
        val optionsBuilder = PhoneAuthOptions.newBuilder(auth)
            .setPhoneNumber(phoneNumber)       // Phone number to verify
            .setTimeout(55L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(this)                 // Activity (for callback binding)
            .setCallbacks(callbacks)          // OnVerificationStateChangedCallbacks
        if (token != null) {
            optionsBuilder.setForceResendingToken(token) // callback's ForceResendingToken
        }
        PhoneAuthProvider.verifyPhoneNumber(optionsBuilder.build())
    }
    // [END resend_verification]

    // count dow timer
    private fun startTimer() {
        val timer = object : CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                binding.txtCountDownOTP.text = (millisUntilFinished / 1000).toString()
            }

            override fun onFinish() {
                binding.txtResendCodeOTP.visible()
                binding.llTimerOTP.gone()
            }
        }
        timer.start()
    }
}