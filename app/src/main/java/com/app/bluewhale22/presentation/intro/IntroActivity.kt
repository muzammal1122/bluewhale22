package com.app.bluewhale22.presentation.intro

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.app.bluewhale22.BaseActivity
import com.app.bluewhale22.R
import com.app.bluewhale22.databinding.ActivityIntroBinding
import com.app.bluewhale22.presentation.login.PhoneNumberActivity

class IntroActivity : BaseActivity() {

    lateinit var binding: ActivityIntroBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_intro)

             binding.ivNextIntro.setOnClickListener {
            val intent = Intent(this, PhoneNumberActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
}