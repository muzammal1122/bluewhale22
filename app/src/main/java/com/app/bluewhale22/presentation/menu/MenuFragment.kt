package com.app.bluewhale22.presentation.menu

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.lifecycle.lifecycleScope
import com.app.bluewhale22.R
import com.app.bluewhale22.databinding.MenuFragmentBinding
import com.app.bluewhale22.presentation.login.PhoneNumberActivity
import com.app.bluewhale22.presentation.menu.contactus.ContactUsActivity
import com.app.bluewhale22.presentation.menu.profile.ProfileFragment
import com.app.bluewhale22.presentation.parent.ParentActivity
import com.app.bluewhale22.utils.Constants
import com.app.bluewhale22.utils.SharedPrefs
import com.app.bluewhale22.utils.Utils
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject


@AndroidEntryPoint
class MenuFragment : Fragment() {

    @Inject
    lateinit var preferences: DataStore<Preferences>
    lateinit var binding: MenuFragmentBinding
    private lateinit var langSharedPrefs: SharedPrefs
    var isEnglishSelected: Boolean = false


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.menu_fragment, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        lifecycleScope.launch {
//            binding.txtName.text =
//                Utils.getStringValue(preferences, Constants.FNAME).first() ?: "--"
//            binding.txtEmail.text =
//                Utils.getStringValue(preferences, Constants.EMAIL).first() ?: "--"
//
//
//        }

        // go to notification screen
        binding.imgNotificationMenu.setOnClickListener {
            showToast(getString(R.string.no_notifications))

        }

        binding.appLanguageEnglish.setOnClickListener {
            langSharedPrefs.saveLanguage("en")
            val intent = Intent(activity, ParentActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }

        binding.appLanguageArabic.setOnClickListener {
            langSharedPrefs.saveLanguage("ar")
            val intent = Intent(activity, ParentActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }


        binding.txtVersionCode.text =
            context?.packageManager?.getPackageInfo(requireContext().packageName, 0)?.versionName
                ?: "1.1"

        binding.privacyPolicy.setOnClickListener {
            goToWebViewScreen("https://www.iubenda.com/privacy-policy/63112702")
        }

        // KYC
        binding.faq.setOnClickListener {
            goToWebViewScreen("https://www.bluewhale.host/kyc.php")
        }

        binding.contactUs.setOnClickListener {
            startActivity(Intent(activity, ContactUsActivity::class.java))
        }

        binding.appLanguage.setOnClickListener {

            if (binding.llLanguageLayout.visibility == View.VISIBLE) {
                binding.llLanguageLayout.visibility = View.GONE
                binding.imageUpLang.setImageDrawable(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.ic_baseline_navigate_next_24
                    )
                )
            } else {
                binding.llLanguageLayout.visibility = View.VISIBLE
                binding.imageUpLang.setImageDrawable(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.ic_baseline_keyboard_arrow_down_24
                    )
                )
            }
        }

        binding.rlProfileMF.setOnClickListener {

            fragmentManager?.commit {
                setReorderingAllowed(true)
                replace(R.id.fragment_container_view, ProfileFragment())
            }
        }

        binding.btnSignOut.setOnClickListener {
            lifecycleScope.launch {
                Utils.addStringValue(preferences, Constants.Login_KEY, "no code")
            }
            val intent = Intent(activity, PhoneNumberActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }


    }

    private fun showToast(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        langSharedPrefs = SharedPrefs(requireContext())
        initLanguageSelection()

    }

    private fun goToWebViewScreen(url: String) {
        val intent = Intent(activity, PrivacyPolicyActivity::class.java)
        intent.putExtra("myUrl", url)
        startActivity(intent)
    }

    private fun initLanguageSelection() {
        val selectedLanguage = langSharedPrefs.getLanguage()
        if (selectedLanguage == "en") {
            makeEnglishSelected()
        } else {
            makeArabicSelected()
        }
    }

    private fun makeEnglishSelected() {
        binding.txtSelectedLanguage.text = resources.getString(R.string.english)
        binding.txtEnglishLanguagge.typeface =
            ResourcesCompat.getFont(requireContext(), R.font.raleway_bold)
        binding.txtArabicLanguage.typeface =
            ResourcesCompat.getFont(requireContext(), R.font.raleway_regular)
        isEnglishSelected = true
    }

    private fun makeArabicSelected() {
        binding.txtSelectedLanguage.text = resources.getString(R.string.arabic)
        binding.txtEnglishLanguagge.typeface =
            ResourcesCompat.getFont(requireContext(), R.font.raleway_regular)
        binding.txtArabicLanguage.typeface =
            ResourcesCompat.getFont(requireContext(), R.font.raleway_bold)
        isEnglishSelected = false
    }

}