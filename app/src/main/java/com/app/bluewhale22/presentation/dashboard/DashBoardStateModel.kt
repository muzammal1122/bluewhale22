package com.app.bluewhale22.presentation.dashboard

import com.app.bluewhale22.domain.dashboard.entity.DashBoardApiResponse

sealed class DashBoardStateModel {

    object Init : DashBoardStateModel()
    data class IsLoading(val isLoading: Boolean) : DashBoardStateModel()
    data class Response(val response: DashBoardApiResponse) : DashBoardStateModel()
    data class FoundException(val exception: Exception) : DashBoardStateModel()
    data class GenericError(val message: String) : DashBoardStateModel()
    data class StatusFailed(val message: String) : DashBoardStateModel()
}