package com.app.bluewhale22.presentation.home

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.domain.home.entity.AddMoreEntity
import com.app.bluewhale22.domain.home.usecase.AddMoreUseCase
import com.app.bluewhale22.domain.home.usecase.HomeApiUseCase
import com.app.bluewhale22.utils.Constants
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject


class HomeViewModel @Inject constructor(private val useCase: HomeApiUseCase, private val useCaseAdd : AddMoreUseCase) : ViewModel() {

    private val state = MutableStateFlow<HomeStateModel>(HomeStateModel.Init)
    val mState: StateFlow<HomeStateModel> get() = state
    private val TAG = "HomeViewModel"
    private fun setLoading() {

        state.value = HomeStateModel.IsLoading(true)
    }

    private fun hideLoading() {
        Log.d(TAG, " Called on start")
        state.value = HomeStateModel.IsLoading(false)
    }

    fun getHomeData(phone: String) {
        viewModelScope.launch {
            useCase.execute(HomeApiUseCase.Params(phone))
                .onStart {

                    setLoading()
                }
                .collect {
                    hideLoading()
                    Log.d(TAG, " Called collect")
                    when (it) {
                        is DataState.GenericError -> {
                            Log.d(TAG, "Enter generic error")
                            val message =
                                it.error?.errorResponse?.errorMessage ?: Constants.UNKNOWN_ERROR
                            Log.d(TAG, "Message: $message")
                            state.value = HomeStateModel.GenericError(message)

                        }

                        is DataState.Success -> {
                            Log.d(TAG, "Enter SUCCESS")
                            val status = it.value.response?.status ?: "Unknown"
                            if (status == "FAILED") {
                                val message = it.value.response?.message ?: "Unknown FAILED message"
                                Log.d(TAG, message)
                                state.value = HomeStateModel.StatusFailed(message)
                            } else {
                                Log.d(TAG, "  Success")
                                state.value = it.value.data?.let { it1 ->
                                    HomeStateModel.Response(it1)
                                }!!
                            }

                        }
                    }
                }
        }

    }

    fun addMore(entity : AddMoreEntity) {
        viewModelScope.launch {
            useCaseAdd.execute(AddMoreUseCase.Params(entity))
                .onStart {
                    setLoading()
                }
                .collect {
                    when (it) {
                        is DataState.GenericError -> {
                            Log.d(TAG, "Enter ADD more generic error")
                            val message =
                                it.error?.errorResponse?.errorMessage ?: Constants.UNKNOWN_ERROR
                            Log.d(TAG, "Message: $message")
                            state.value = HomeStateModel.GenericError(message)

                        }

                        is DataState.Success -> {
                            Log.d(TAG, "Enter ADD more SUCCESS ")
                            val status = it.value.response?.status ?: "Unknown"
                            if (status == "FAILED") {
                                val message = it.value.response?.message ?: "Unknown FAILED message"
                                Log.d(TAG, message)
                                state.value = HomeStateModel.StatusFailed(message)
                            } else {
                                Log.d(TAG, "ADD more  Success")
                                state.value = it.value.data?.let { it1 ->
                                    Log.d(TAG, it1.total_balance)
                                    HomeStateModel.ResponseAdd(it1)
                                }!!
                            }

                        }
                    }
                }
        }
    }
}