package com.app.bluewhale22.presentation.parent

import android.content.Context
import android.os.Bundle
import android.view.MenuItem
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit
import androidx.lifecycle.lifecycleScope
import com.app.bluewhale22.BaseActivity
import com.app.bluewhale22.R
import com.app.bluewhale22.databinding.ParentHomeBinding
import com.app.bluewhale22.presentation.dashboard.DashBoardFragment
import com.app.bluewhale22.presentation.home.HomeFragment
import com.app.bluewhale22.presentation.menu.MenuFragment
import com.app.bluewhale22.presentation.news.NewsFragment
import com.app.bluewhale22.presentation.referral.ReferralFragment
import com.app.bluewhale22.utils.Constants
import com.app.bluewhale22.utils.Utils
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ParentActivity : BaseActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    private lateinit var fragmentManager: FragmentManager
    private lateinit var binding: ParentHomeBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ParentHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        lifecycleScope.launch {
            phone = Utils.getStringValue(preferences, Constants.PHONE).first() ?: "123456"
        }

        binding.bottomNavigation.setOnNavigationItemSelectedListener(this)
        fragmentManager = supportFragmentManager
        replaceFragment(HomeFragment())

    }

    companion object {
        var phone: String = "123456"
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.ic_home -> replaceFragment(HomeFragment())
            R.id.ic_logo -> replaceFragment(DashBoardFragment())
            R.id.ic_news -> replaceFragment(NewsFragment())
            R.id.ic_user -> replaceFragment(MenuFragment())
            else -> replaceFragment(ReferralFragment())
        }
        return true
    }

    private  fun replaceFragment(fragment: Fragment) {
        fragmentManager.commit {
            setReorderingAllowed(true)
            replace(R.id.fragment_container_view, fragment)
        }
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }

}