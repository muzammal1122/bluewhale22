package com.app.bluewhale22.presentation.password

import `in`.aabhasjindal.otptextview.OTPListener
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import com.app.bluewhale22.BaseActivity
import com.app.bluewhale22.R
import com.app.bluewhale22.databinding.ActivityPasswordBinding
import com.app.bluewhale22.presentation.login.PhoneNumberActivity
import com.app.bluewhale22.presentation.parent.ParentActivity
import com.app.bluewhale22.utils.Constants
import com.app.bluewhale22.utils.Utils
import com.app.bluewhale22.utils.visible
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch

@AndroidEntryPoint
class PasswordActivity : BaseActivity() {


    private lateinit var userType: String
    private lateinit var code: String
    lateinit var binding: ActivityPasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_password)

        userType = intent.extras?.getString(Constants.USER_TYPE) ?: Constants.NEW_USER

        binding.otpViewPA.resetState()


        lifecycleScope.launch {
            code = Utils.getStringValue(preferences, Constants.Login_KEY).first() ?: "1001"
        }

        if (userType == Constants.NEW_USER) {
            binding.txtTitleTextPA.text = getString(R.string.create_password)
            binding.txtTitle.text = getString(R.string.please_set_your_login_code)
            binding.txtForgetPA.visibility = GONE
        } else {
            binding.txtTitleTextPA.text = getString(R.string.login_code)
            binding.txtTitle.text = getString(R.string.please_enter_your_login_code)
            binding.txtForgetPA.visibility = View.VISIBLE
        }

        //when otp is done

        binding.otpViewPA.otpListener = object : OTPListener {
            override fun onInteractionListener() {

            }

            override fun onOTPComplete(otp: String) {
                hideKeyBoard()
                goToNext(otp)
            }
        }

        binding.txtForgetPA.setOnClickListener {
            lifecycleScope.launch {
                Utils.addStringValue(preferences, Constants.Login_KEY, "no code")
            }
            val intent = Intent(this, PhoneNumberActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }

        binding.backArrow.setOnClickListener {
            if (userType == Constants.NEW_USER){
                val intent = Intent(this, PhoneNumberActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }else
            finish()
        }


    }

    private fun goToNext(otp: String) {
        if (userType == Constants.NEW_USER) {
            val intent = Intent(this, ConfirmPasswordActivity::class.java)
            intent.putExtra("loginCode", otp)
            intent.putExtra(Constants.USER_TYPE, userType)
            startActivity(intent)
        } else {
            if (otp == code) {
                val intent = Intent(this, ParentActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            } else {
                binding.txtInvalidCode.visible()
                binding.otpViewPA.showError()
            }
        }
    }

    private fun hideKeyBoard() {
        val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(View(this).windowToken, 0)
    }
}