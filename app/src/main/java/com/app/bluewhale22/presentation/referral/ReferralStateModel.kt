package com.app.bluewhale22.presentation.referral

import com.app.bluewhale22.domain.referral.entity.ReferralApiResponse
import com.app.bluewhale22.domain.referral.entity.ReferralCodeResponse
import com.app.bluewhale22.domain.referral.entity.ReferralStatusResponse

sealed class ReferralStateModel {
    object Init : ReferralStateModel()
    data class IsLoading(val isLoading: Boolean) : ReferralStateModel()
    data class Response(val response: ReferralApiResponse) : ReferralStateModel()
    data class CodeResponse(val response: ReferralCodeResponse) : ReferralStateModel()
    data class StatusResponse(val response: ReferralStatusResponse) : ReferralStateModel()
    data class FoundException(val exception: Exception) : ReferralStateModel()
    data class GenericError(val message: String) : ReferralStateModel()
    data class StatusFailed(val message: String) : ReferralStateModel()
}
