package com.app.bluewhale22.utils

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.Window
import androidx.appcompat.widget.AppCompatButton
import com.app.bluewhale22.R

class ShowInfoDialog(context: Context) : Dialog(context) {

    init {
        dialog = Dialog(context)
    }

    fun showPopup() {

        val dialogview = LayoutInflater.from(context)
            .inflate(R.layout.info_dialog, null, false)

        //initializing dialog screen
        val btnGot: AppCompatButton = dialogview.findViewById(R.id.btnGotItInfo)

        btnGot.setOnClickListener {
            dismissPopup()
        }


        dialog?.setCancelable(false)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog?.setContentView(dialogview)
        dialog?.show()

    }

    companion object {
        var dialog: Dialog? = null
        fun dismissPopup() = dialog?.let { dialog!!.dismiss() }
    }
}