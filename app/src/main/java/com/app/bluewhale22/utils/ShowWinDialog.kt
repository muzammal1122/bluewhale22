package com.app.bluewhale22.utils

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.Window
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import com.app.bluewhale22.R

class ShowWinDialog(context: Context, val bal: String, var listener: WinDialogListener) :
    Dialog(context) {

    init {
        dialog = Dialog(context)
    }

    fun showPopup() {

        val dialogview = LayoutInflater.from(context)
            .inflate(R.layout.win_home_dialog, null, false)
        //initializing dialog screen
        val btnGotIt: AppCompatButton = dialogview.findViewById(R.id.btnGotIt)
        val textView: TextView = dialogview.findViewById(R.id.txtBalance)

        textView.text = Utils.numberFormat(bal.toLong(), context)

        btnGotIt.setOnClickListener {
            listener.onDialogHide()
            dismissPopup()
        }

        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog?.setCancelable(false)
        dialog?.setContentView(dialogview)
        dialog?.show()

    }

    companion object {
        var dialog: Dialog? = null
        fun dismissPopup() = dialog?.let { dialog!!.dismiss() }
    }

}

interface WinDialogListener {
    fun onDialogHide()
}