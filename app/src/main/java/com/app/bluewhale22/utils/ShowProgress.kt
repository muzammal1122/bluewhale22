package com.app.bluewhale22.utils

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.Window
import android.widget.CheckBox
import androidx.appcompat.widget.AppCompatButton
import com.app.bluewhale22.R

class ShowProgress(context: Context, var listener: ProgressDialogListener) : Dialog(context) {

    init {
        dialog = Dialog(context)
    }

    fun showPopup() {

        val dialogview = LayoutInflater.from(context)
            .inflate(R.layout.custom_dialog_home, null, false)

        //initializing dialog screen
        val btnAccept: AppCompatButton = dialogview.findViewById(R.id.btnAccept)
        val cbMessage: CheckBox = dialogview.findViewById(R.id.cbMessage)

        btnAccept.setOnClickListener {
            if (cbMessage.isChecked) {
                listener.onDontShowCheck()
            }
            dismissPopup()
        }


        dialog?.setCancelable(false)
       dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog?.setContentView(dialogview)
        dialog?.show()

    }

    companion object {
        var dialog: Dialog? = null
        fun dismissPopup() = dialog?.let { dialog!!.dismiss() }
    }

}

interface ProgressDialogListener {
    fun onDontShowCheck()
}