package com.app.bluewhale22.utils

import android.content.Context
import android.graphics.Bitmap
import android.widget.Toast
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.*
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import java.lang.NumberFormatException
import java.text.DecimalFormat

object Utils {
    suspend fun addStringValue(preferences: DataStore<Preferences>, key: String, value: String) {
        preferences.edit {
            it[stringPreferencesKey(key)] = value
        }
    }

    suspend fun addBooleanValue(preferences: DataStore<Preferences>, key: String, value: Boolean) {
        preferences.edit {
            it[booleanPreferencesKey(key)] = value
        }
    }

    suspend fun addDoubleValue(preferences: DataStore<Preferences>, key: String, value: Double) {
        preferences.edit {
            it[doublePreferencesKey(key)] = value
        }
    }

    suspend fun getStringValue(preferences: DataStore<Preferences>, key: String) = flow {
        val dataStoreKey = stringPreferencesKey(key)
        val preferences = preferences.data.first()
        emit(preferences[dataStoreKey])
    }

    suspend fun getBooleanValue(preferences: DataStore<Preferences>, key: String) = flow {
        val dataStoreKey = booleanPreferencesKey(key)
        val preferences = preferences.data.first()
        emit(preferences[dataStoreKey])
    }

    suspend fun getDoubleValue(preferences: DataStore<Preferences>, key: String) = flow {
        val dataStoreKey = doublePreferencesKey(key)
        val preferences = preferences.data.first()
        emit(preferences[dataStoreKey])
    }


        fun generateQRCode(referralCode: String): Bitmap? {
            return BarcodeEncoder().encodeBitmap(
                "https://www.bluewhale.host/ref.php?refcode=$referralCode",
                BarcodeFormat.QR_CODE, 600, 600
            )
        }

        fun numberFormat(number: Long, context: Context): String? {
            return try {
                val formatter: DecimalFormat = DecimalFormat("#,###,###")
                formatter.format(number)
            } catch (e: NumberFormatException) {
               // Toast.makeText(context, "Values wrong formatted", Toast.LENGTH_SHORT).show()
                null
            } catch (e1: Exception) {
                Toast.makeText(context, e1.message, Toast.LENGTH_SHORT).show()
                null
            }
        }

    }
