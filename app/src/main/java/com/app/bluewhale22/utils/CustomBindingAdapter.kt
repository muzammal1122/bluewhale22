package com.app.bluewhale22.utils

import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.BindingAdapter
import com.app.bluewhale22.R
import com.app.bluewhale22.utils.Utils.numberFormat
import com.bumptech.glide.Glide
import java.lang.NumberFormatException

@BindingAdapter ("imageFromUrl")
fun ImageView.imageFromUrl(url:String?){
    Glide
        .with(this.context)
        .load(url)
        .placeholder(R.drawable.placeholder)
        .into(this)
}

@BindingAdapter("languagePrefImage")
fun ImageView.languagePrefImage(isTrue:Boolean){
    val pref = SharedPrefs(this.context)
    if(pref.getLanguage() == "en"){
        this.setImageResource(R.drawable.ic_baseline_navigate_next_24)
    }else{
        this.setImageResource(R.drawable.ic_baseline_navigate_before)
    }
}
@BindingAdapter("customBackground")
fun LinearLayout.customBackground(isSelected:Boolean = false){
    if(isSelected){
        this.background = this.context.getDrawable(R.drawable.rectange_round_border_light_blue)
    }else{
        this.background = this.context.getDrawable(R.drawable.rectange_round_border_white)
    }
}

@BindingAdapter("customColor")
fun TextView.customColor(isSelected:Boolean = false){
    if(isSelected){
        this.setTextColor(resources.getColor(R.color.white))
    }else {
        this.setTextColor(resources.getColor(R.color.darkBlue))
    }
}
@BindingAdapter ("formatedString")
fun TextView.formatedString(number:String?){
    if(number!=null){
        try {
            val numbered = java.lang.Long.valueOf(number)
            this.text = numberFormat(numbered, this.context)
        }catch (e: NumberFormatException){
            Toast.makeText(context,"Values wrong formatted", Toast.LENGTH_SHORT).show()
            this.text = "--"

        }catch (e1:Exception){
            Toast.makeText(context,e1.message, Toast.LENGTH_SHORT).show()
            this.text = "--"
        }
    }



}