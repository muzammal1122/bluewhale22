package com.app.bluewhale22.domain.menu.entity

import com.google.gson.annotations.SerializedName

data class EmailEntity(
    @SerializedName("email") val email : String,
    @SerializedName("body") val msg : String
)
