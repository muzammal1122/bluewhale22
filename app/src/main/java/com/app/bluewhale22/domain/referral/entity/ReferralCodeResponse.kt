package com.app.bluewhale22.domain.referral.entity

import com.google.gson.annotations.SerializedName

data class ReferralCodeResponse(
    @SerializedName("referral_code") val code : String
)
