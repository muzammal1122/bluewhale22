package com.app.bluewhale22.domain.home

import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.domain.home.entity.AddMoreEntity
import com.app.bluewhale22.domain.home.entity.AddMoreResponse
import com.app.bluewhale22.domain.home.entity.HomeApiResponse
import kotlinx.coroutines.flow.Flow

interface HomeApiRepo {

    suspend fun getHomeResponse(phone: String): Flow<DataState<WrappedResponse<HomeApiResponse>>>

    suspend fun addMoreResponse(
        url: String,
        entity: AddMoreEntity
    ): Flow<DataState<WrappedResponse<AddMoreResponse>>>
}