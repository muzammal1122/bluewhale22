package com.app.bluewhale22.domain.signUp

import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.domain.signUp.entity.SignUpApiResponse
import com.app.bluewhale22.domain.signUp.entity.SignUpEntity
import kotlinx.coroutines.flow.Flow

interface SignUpRepository {

  suspend fun signUpResponse(entity:SignUpEntity) : Flow<DataState<WrappedResponse<SignUpApiResponse>>>
}