package com.app.bluewhale22.domain.referral.usecase

import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.data.common.utils.NetworkHelper
import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.domain.referral.ReferralRepo
import com.app.bluewhale22.domain.referral.entity.ReferralStatusEntity
import com.app.bluewhale22.domain.referral.entity.ReferralStatusResponse
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ReferralStatusUseCase @Inject constructor(private val repo: ReferralRepo) :
    NetworkHelper<ReferralStatusUseCase.Params, WrappedResponse<ReferralStatusResponse>>() {

    data class Params(val entity: ReferralStatusEntity)

    override suspend fun buildUseCase(parameter: Params): Flow<DataState<WrappedResponse<ReferralStatusResponse>>> {
        return repo.getReferralStatus("", parameter.entity)
    }
}
