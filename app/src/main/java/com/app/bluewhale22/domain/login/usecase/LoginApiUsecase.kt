package com.app.bluewhale22.domain.login.usecase

import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.data.common.utils.NetworkHelper
import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.domain.login.LoginRepository
import com.app.bluewhale22.domain.login.entity.LoginApiResponse
import com.app.bluewhale22.domain.login.entity.LoginEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class LoginApiUsecase @Inject constructor(private val loginRepository: LoginRepository) :
    NetworkHelper<LoginApiUsecase.Params, WrappedResponse<LoginApiResponse>>() {
    class Params(val entity: LoginEntity)

    override suspend fun buildUseCase(parameter: Params): Flow<DataState<WrappedResponse<LoginApiResponse>>> {
        return loginRepository.loginResponse("",parameter.entity)
    }
}
