package com.app.bluewhale22.domain.home.entity

import com.google.gson.annotations.SerializedName

data class AddMoreEntity(@SerializedName("booking_amount") val amount: Int,
                         @SerializedName("phone_number") val pone: String)
