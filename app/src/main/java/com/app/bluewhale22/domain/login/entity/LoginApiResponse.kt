package com.app.bluewhale22.domain.login.entity

import com.google.gson.annotations.SerializedName

data class LoginApiResponse(
    @SerializedName("last_name") var last_name : String,
    @SerializedName("email") var email : String,
    @SerializedName("first_name") var first_name : String,
    @SerializedName("referral_code") var code : String
)
