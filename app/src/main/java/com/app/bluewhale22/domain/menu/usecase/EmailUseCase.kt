package com.app.bluewhale22.domain.menu.usecase

import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.data.common.utils.NetworkHelper
import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.domain.menu.MenuRepo
import com.app.bluewhale22.domain.menu.entity.EmailEntity
import com.app.bluewhale22.domain.menu.entity.EmailResponse
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class EmailUseCase @Inject constructor(val repo: MenuRepo) :
    NetworkHelper<EmailUseCase.Params, WrappedResponse<EmailResponse>>() {

    data class Params(val entity: EmailEntity)

    override suspend fun buildUseCase(parameter: Params): Flow<DataState<WrappedResponse<EmailResponse>>> {
        return repo.getEmailResponse("", parameter.entity)
    }
}