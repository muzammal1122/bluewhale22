package com.app.bluewhale22.domain.referral.usecase

import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.data.common.utils.NetworkHelper
import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.domain.referral.ReferralRepo
import com.app.bluewhale22.domain.referral.entity.ReferralApiResponse
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ReferralApiUseCase @Inject constructor(private val repo: ReferralRepo) :
    NetworkHelper<ReferralApiUseCase.Params, WrappedResponse<ReferralApiResponse>>() {

    data class Params(val phone: String)

    override suspend fun buildUseCase(parameter: Params): Flow<DataState<WrappedResponse<ReferralApiResponse>>> {
        return repo.getReferralCode(parameter.phone)
    }
}