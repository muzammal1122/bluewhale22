package com.app.bluewhale22.domain.menu.usecase

import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.data.common.utils.NetworkHelper
import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.domain.menu.MenuRepo
import com.app.bluewhale22.domain.menu.entity.ProfileEntity
import com.app.bluewhale22.domain.menu.entity.UpdateProfileResponse
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class UpdateProfileApiUsecase @Inject constructor(private val repo : MenuRepo) :
    NetworkHelper<UpdateProfileApiUsecase.Params, WrappedResponse<UpdateProfileResponse>>() {

    data class Params(val phone : String, val entity : ProfileEntity)

    override suspend fun buildUseCase(parameter: Params): Flow<DataState<WrappedResponse<UpdateProfileResponse>>> {
        return repo.updateProfileResponse(parameter.phone, parameter.entity)
    }
}