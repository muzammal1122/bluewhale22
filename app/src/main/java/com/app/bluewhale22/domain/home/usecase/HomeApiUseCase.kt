package com.app.bluewhale22.domain.home.usecase

import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.data.common.utils.NetworkHelper
import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.domain.home.HomeApiRepo
import com.app.bluewhale22.domain.home.entity.HomeApiResponse
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class HomeApiUseCase @Inject constructor(private val repo: HomeApiRepo) :
    NetworkHelper<HomeApiUseCase.Params, WrappedResponse<HomeApiResponse>>() {

    data class Params(val phone: String)

    override suspend fun buildUseCase(parameter: Params): Flow<DataState<WrappedResponse<HomeApiResponse>>> {
        return repo.getHomeResponse(parameter.phone)
    }
}
