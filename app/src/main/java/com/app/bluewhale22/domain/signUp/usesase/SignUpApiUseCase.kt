package com.app.bluewhale22.domain.signUp.usesase

import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.data.common.utils.NetworkHelper
import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.domain.signUp.SignUpRepository
import com.app.bluewhale22.domain.signUp.entity.SignUpApiResponse
import com.app.bluewhale22.domain.signUp.entity.SignUpEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class SignUpApiUseCase @Inject constructor(private val signUpRepository: SignUpRepository) : NetworkHelper<SignUpApiUseCase.Params, WrappedResponse<SignUpApiResponse>>() {

     data class Params(val entity: SignUpEntity)

    override suspend fun buildUseCase(parameter: Params): Flow<DataState<WrappedResponse<SignUpApiResponse>>> {
        return signUpRepository.signUpResponse(parameter.entity)
    }
}