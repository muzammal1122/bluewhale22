package com.app.bluewhale22.domain.login.entity

import com.google.gson.annotations.SerializedName

data class LoginEntity(
    @SerializedName("notification_token") val token: String,
    @SerializedName("phone_number") val phone: String
)
