package com.app.bluewhale22.domain.home.entity

import com.google.gson.annotations.SerializedName

data class HomeApiResponse(
    @SerializedName("bw22_total_balance") val total_balance: String,
    @SerializedName("user_usdt_totoal_balance") val usdt: String,
    @SerializedName("usdt_value") val usdt_value: String,
    @SerializedName("market_total_pre_booking_value") val preBooking: String,
    @SerializedName("market_cap_usdt_value") val market_cap: String,
    @SerializedName("change_in_24_hours_percentage") val percentage: String,
    @SerializedName("is_up") val arrowPosition: Boolean,
    @SerializedName("total_pre_booked_amount") val total_pre_booked_amount: String
)

data class AddMoreResponse(
    @SerializedName("bw22_total_balance") val total_balance: String,
    @SerializedName("user_usdt_totoal_balance") val usdt: String,
    @SerializedName("usdt_value") val usdt_value: String,
    @SerializedName("market_total_pre_booking_value") val preBooking: String,
    @SerializedName("market_cap_usdt_value") val market_cap: String,
    @SerializedName("change_in_24_hours_percentage") val percentage: String,
    @SerializedName("is_up") val arrowPosition: Boolean,
    @SerializedName("total_pre_booked_amount") val total_pre_booked_amount: String
)
