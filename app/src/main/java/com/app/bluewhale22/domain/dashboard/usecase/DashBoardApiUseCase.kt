package com.app.bluewhale22.domain.dashboard.usecase

import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.data.common.utils.NetworkHelper
import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.domain.dashboard.DashBoardApiRepo
import com.app.bluewhale22.domain.dashboard.entity.DashBoardApiResponse
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class DashBoardApiUseCase @Inject constructor(private val repo : DashBoardApiRepo ) :
    NetworkHelper<DashBoardApiUseCase.Params, WrappedResponse<DashBoardApiResponse>>() {

     class Params()

    override suspend fun buildUseCase(parameter: Params): Flow<DataState<WrappedResponse<DashBoardApiResponse>>> {
        return repo.getDashBoardApiResponse("")
    }
}