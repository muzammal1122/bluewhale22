package com.app.bluewhale22.domain.referral.entity

import com.google.gson.annotations.SerializedName

data class ReferralStatusResponse(
    @SerializedName("message") val status: String
)
