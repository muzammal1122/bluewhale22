package com.app.bluewhale22.domain.menu

import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.domain.menu.entity.EmailEntity
import com.app.bluewhale22.domain.menu.entity.EmailResponse
import com.app.bluewhale22.domain.menu.entity.ProfileEntity
import com.app.bluewhale22.domain.menu.entity.UpdateProfileResponse
import kotlinx.coroutines.flow.Flow

interface MenuRepo {

    suspend fun updateProfileResponse(phone : String, entity : ProfileEntity) : Flow<DataState<WrappedResponse<UpdateProfileResponse>>>

    suspend fun getEmailResponse(url : String, entity : EmailEntity) : Flow<DataState<WrappedResponse<EmailResponse>>>
}