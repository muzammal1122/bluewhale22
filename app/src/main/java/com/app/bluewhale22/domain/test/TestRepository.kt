package com.app.bluewhale22.domain.test

import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.domain.test.entity.TestApiResponse
import kotlinx.coroutines.flow.Flow

interface TestRepository {
    suspend fun testResponse(): Flow<DataState<WrappedResponse<TestApiResponse>>>
    suspend fun xyz():String
}