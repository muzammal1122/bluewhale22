package com.app.bluewhale22.domain.dashboard.entity

import com.google.gson.annotations.SerializedName

data class DashBoardApiResponse(
    @SerializedName("market_cap_usdt_value") val market_price: Double,
    @SerializedName("change_in_24_hours_market_cap_percentage") val market_percentage: Double,
    @SerializedName("market_movement") val market_movement: Map<String, Double>
)