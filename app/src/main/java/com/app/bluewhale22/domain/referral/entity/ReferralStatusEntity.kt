package com.app.bluewhale22.domain.referral.entity

import com.google.gson.annotations.SerializedName

data class ReferralStatusEntity(
    @SerializedName("referral_code") val code: String,
    @SerializedName("phone_number") val phone: String
)
