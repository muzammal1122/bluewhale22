package com.app.bluewhale22.domain.login

import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.domain.login.entity.LoginApiResponse
import com.app.bluewhale22.domain.login.entity.LoginEntity
import kotlinx.coroutines.flow.Flow


interface LoginRepository {
    suspend fun loginResponse(url: String, entity : LoginEntity) : Flow<DataState<WrappedResponse<LoginApiResponse>>>
}