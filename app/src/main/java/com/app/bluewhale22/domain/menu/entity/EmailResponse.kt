package com.app.bluewhale22.domain.menu.entity

import com.google.gson.annotations.SerializedName

data class EmailResponse(
    @SerializedName("message") val msg : String,
    @SerializedName("messageID") val id : String
)
