package com.app.bluewhale22.domain.signUp.entity

import com.google.gson.annotations.SerializedName

data class SignUpEntity(
    @SerializedName("email") val email: String,
    @SerializedName("image") val image: String?,
    @SerializedName("first_name") val first_name: String,
    @SerializedName("last_name") val last_name: String,
    @SerializedName("phone_number") val phone: String,
    @SerializedName("notification_token") val token: String
)