package com.app.bluewhale22.domain.referral

import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.domain.referral.entity.ReferralApiResponse
import com.app.bluewhale22.domain.referral.entity.ReferralCodeResponse
import com.app.bluewhale22.domain.referral.entity.ReferralStatusEntity
import com.app.bluewhale22.domain.referral.entity.ReferralStatusResponse
import kotlinx.coroutines.flow.Flow

interface ReferralRepo {

   suspend fun getReferralCode( url : String) : Flow<DataState<WrappedResponse<ReferralApiResponse>>>

   suspend fun getMyReferralCode( url : String) : Flow<DataState<WrappedResponse<ReferralCodeResponse>>>

   suspend fun getReferralStatus( url : String, entity: ReferralStatusEntity) : Flow<DataState<WrappedResponse<ReferralStatusResponse>>>
}