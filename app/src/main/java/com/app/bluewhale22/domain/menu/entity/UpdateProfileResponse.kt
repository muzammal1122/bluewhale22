package com.app.bluewhale22.domain.menu.entity

import com.google.gson.annotations.SerializedName

data class UpdateProfileResponse(
    @SerializedName("message") val message:String

)
