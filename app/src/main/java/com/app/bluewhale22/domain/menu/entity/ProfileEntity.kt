package com.app.bluewhale22.domain.menu.entity

import com.google.gson.annotations.SerializedName

data class ProfileEntity(
    @SerializedName("first_name") val firstName : String,
    @SerializedName("last_name") val lastName : String,
    @SerializedName("address") val address : String
)
