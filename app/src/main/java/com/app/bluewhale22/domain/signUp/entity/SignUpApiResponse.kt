package com.app.bluewhale22.domain.signUp.entity

import com.google.gson.annotations.SerializedName

data class SignUpApiResponse(
    @SerializedName("message") val message: String,
    @SerializedName("referral_code") val code: String
)
