package com.app.bluewhale22.domain.test.usecase

import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.data.common.utils.NetworkHelper
import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.domain.test.TestRepository
import com.app.bluewhale22.domain.test.entity.TestApiResponse
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class TestApiUseCase @Inject constructor(private val testRepository:TestRepository): NetworkHelper<TestApiUseCase.Params , WrappedResponse<TestApiResponse>>() {
    class Params

    override suspend fun buildUseCase(parameter: TestApiUseCase.Params): Flow<DataState<WrappedResponse<TestApiResponse>>> {
        return testRepository.testResponse()
    }
}