package com.app.bluewhale22.domain.dashboard

import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.domain.dashboard.entity.DashBoardApiResponse
import kotlinx.coroutines.flow.Flow

interface DashBoardApiRepo {

    suspend fun getDashBoardApiResponse(url : String) : Flow<DataState<WrappedResponse<DashBoardApiResponse>>>
}