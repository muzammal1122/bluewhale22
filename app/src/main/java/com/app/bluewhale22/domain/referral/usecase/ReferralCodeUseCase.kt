package com.app.bluewhale22.domain.referral.usecase

import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.data.common.utils.NetworkHelper
import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.domain.referral.ReferralRepo
import com.app.bluewhale22.domain.referral.entity.ReferralCodeResponse
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ReferralCodeUseCase @Inject constructor(private val repo: ReferralRepo) :
    NetworkHelper<ReferralCodeUseCase.Params, WrappedResponse<ReferralCodeResponse>>() {

     class Params()

    override suspend fun buildUseCase(parameter: Params): Flow<DataState<WrappedResponse<ReferralCodeResponse>>> {
        return repo.getMyReferralCode("")
    }

}