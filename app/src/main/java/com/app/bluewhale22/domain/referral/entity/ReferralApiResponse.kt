package com.app.bluewhale22.domain.referral.entity

import com.google.gson.annotations.SerializedName

data class ReferralApiResponse(
    @SerializedName("referral_code") val code : String,
    @SerializedName("number_of_active_user") val users : String,
    @SerializedName("number_of_followers") val followers : String
)
