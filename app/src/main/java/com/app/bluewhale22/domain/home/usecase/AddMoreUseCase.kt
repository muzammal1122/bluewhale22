package com.app.bluewhale22.domain.home.usecase

import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.data.common.utils.NetworkHelper
import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.domain.home.HomeApiRepo
import com.app.bluewhale22.domain.home.entity.AddMoreEntity
import com.app.bluewhale22.domain.home.entity.AddMoreResponse
import com.app.bluewhale22.domain.home.entity.HomeApiResponse
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class AddMoreUseCase @Inject constructor(private val repo: HomeApiRepo) :
    NetworkHelper<AddMoreUseCase.Params, WrappedResponse<AddMoreResponse>>() {

    data class Params(val entity: AddMoreEntity)

    override suspend fun buildUseCase(parameter: Params): Flow<DataState<WrappedResponse<AddMoreResponse>>> {
        return repo.addMoreResponse("", parameter.entity)
    }


}