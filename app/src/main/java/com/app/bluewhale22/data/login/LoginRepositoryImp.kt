package com.app.bluewhale22.data.login

import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.data.login.remote.LoginApi
import com.app.bluewhale22.domain.login.LoginRepository
import com.app.bluewhale22.domain.login.entity.LoginApiResponse
import com.app.bluewhale22.domain.login.entity.LoginEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class LoginRepositoryImp @Inject constructor(private val loginApi: LoginApi) : LoginRepository {
    override suspend fun loginResponse(
        url: String,
        entity: LoginEntity
    ): Flow<DataState<WrappedResponse<LoginApiResponse>>> = flow {
        emit(DataState.Success(loginApi.getLoginApiResponse("login-user", entity)))
    }


}