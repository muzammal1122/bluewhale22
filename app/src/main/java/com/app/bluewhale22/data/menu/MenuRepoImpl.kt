package com.app.bluewhale22.data.menu

import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.data.menu.remote.MenuApi
import com.app.bluewhale22.domain.menu.MenuRepo
import com.app.bluewhale22.domain.menu.entity.EmailEntity
import com.app.bluewhale22.domain.menu.entity.EmailResponse
import com.app.bluewhale22.domain.menu.entity.ProfileEntity
import com.app.bluewhale22.domain.menu.entity.UpdateProfileResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class MenuRepoImpl @Inject constructor(private val profileApi: MenuApi) : MenuRepo {
    override suspend fun updateProfileResponse(
        phone: String,
        entity: ProfileEntity
    ): Flow<DataState<WrappedResponse<UpdateProfileResponse>>> = flow {
        emit(DataState.Success(profileApi.updateProfile("update-user/$phone", entity)))
    }

    override suspend fun getEmailResponse(
        url: String,
        entity: EmailEntity
    ): Flow<DataState<WrappedResponse<EmailResponse>>> = flow {
        emit(DataState.Success(profileApi.sendEmail("send_email", entity)))
    }
}