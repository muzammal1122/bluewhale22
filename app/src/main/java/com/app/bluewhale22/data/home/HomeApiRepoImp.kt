package com.app.bluewhale22.data.home

import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.data.home.remote.HomeApi
import com.app.bluewhale22.domain.home.HomeApiRepo
import com.app.bluewhale22.domain.home.entity.AddMoreEntity
import com.app.bluewhale22.domain.home.entity.AddMoreResponse
import com.app.bluewhale22.domain.home.entity.HomeApiResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class HomeApiRepoImp @Inject constructor(private val api: HomeApi) : HomeApiRepo {
    override suspend fun getHomeResponse(phone: String): Flow<DataState<WrappedResponse<HomeApiResponse>>> =
        flow {
            emit(DataState.Success(api.getHomeApi("user-stats/$phone")))
        }

    override suspend fun addMoreResponse(
        url: String,
        entity: AddMoreEntity
    ): Flow<DataState<WrappedResponse<AddMoreResponse>>> = flow {
        emit(DataState.Success(api.addMore("add-booking", entity)))
    }


}