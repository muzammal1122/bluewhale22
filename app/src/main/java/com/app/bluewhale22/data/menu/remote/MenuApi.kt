package com.app.bluewhale22.data.menu.remote

import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.domain.menu.entity.EmailEntity
import com.app.bluewhale22.domain.menu.entity.EmailResponse
import com.app.bluewhale22.domain.menu.entity.ProfileEntity
import com.app.bluewhale22.domain.menu.entity.UpdateProfileResponse
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Url

interface MenuApi {

    @PUT
    suspend fun updateProfile(@Url url: String, @Body body : ProfileEntity) : WrappedResponse<UpdateProfileResponse>

    @POST
    suspend fun sendEmail(@Url url: String, @Body body : EmailEntity) : WrappedResponse<EmailResponse>
}