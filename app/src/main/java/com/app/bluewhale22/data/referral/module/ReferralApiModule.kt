package com.app.bluewhale22.data.referral.module

import com.app.bluewhale22.data.referral.ReferralRepoImp
import com.app.bluewhale22.data.referral.remote.ReferralApi
import com.app.bluewhale22.domain.referral.ReferralRepo
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit


@Module
@InstallIn(SingletonComponent::class)
object ReferralApiModule {

    @Provides
    fun providesUpdateProfileRepository(api: ReferralApi): ReferralRepo {
        return ReferralRepoImp(api)
    }

    @Provides
    fun provideUpdateProfileApi(retrofit: Retrofit): ReferralApi {
        return retrofit.create(ReferralApi::class.java)
    }

}