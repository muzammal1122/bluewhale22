package com.app.bluewhale22.data.login.remote

import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.domain.login.entity.LoginApiResponse
import com.app.bluewhale22.domain.login.entity.LoginEntity
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Url

interface LoginApi {

    @PUT
    suspend fun getLoginApiResponse(@Url url : String, @Body body : LoginEntity) : WrappedResponse<LoginApiResponse>
}