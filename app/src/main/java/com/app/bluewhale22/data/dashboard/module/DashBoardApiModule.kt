package com.app.bluewhale22.data.dashboard.module

import com.app.bluewhale22.data.dashboard.DashBoardApiRepoImp
import com.app.bluewhale22.data.dashboard.remote.DashBoardApi
import com.app.bluewhale22.domain.dashboard.DashBoardApiRepo
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent :: class)
object DashBoardApiModule {
    @Provides
    fun providesDashBoardRepository(api: DashBoardApi): DashBoardApiRepo {
        return DashBoardApiRepoImp(api)
    }

    @Provides
    fun provideLoginApi(retrofit: Retrofit): DashBoardApi {
        return retrofit.create(DashBoardApi::class.java)
    }
}