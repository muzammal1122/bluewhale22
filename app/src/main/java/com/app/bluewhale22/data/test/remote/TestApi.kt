package com.app.bluewhale22.data.test.remote

import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.domain.test.entity.TestApiResponse
import retrofit2.http.GET
import retrofit2.http.Url

interface TestApi {
    @GET
    suspend fun getTestApiResponse(@Url url:String):WrappedResponse<TestApiResponse>
}