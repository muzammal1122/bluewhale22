package com.app.bluewhale22.data.home.module

import com.app.bluewhale22.data.home.HomeApiRepoImp
import com.app.bluewhale22.data.home.remote.HomeApi
import com.app.bluewhale22.domain.home.HomeApiRepo
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit


@Module
@InstallIn(SingletonComponent::class)
object HomeApiModule {
    @Provides
    fun providesHomeRepository(api: HomeApi): HomeApiRepo {
        return HomeApiRepoImp(api)
    }

    @Provides
    fun provideHomeApi(retrofit: Retrofit): HomeApi {
        return retrofit.create(HomeApi::class.java)
    }

}