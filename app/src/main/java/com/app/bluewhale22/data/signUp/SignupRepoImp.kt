package com.app.bluewhale22.data.signUp

import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.data.signUp.remote.SignUpApi
import com.app.bluewhale22.domain.signUp.SignUpRepository
import com.app.bluewhale22.domain.signUp.entity.SignUpApiResponse
import com.app.bluewhale22.domain.signUp.entity.SignUpEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class SignupRepoImp @Inject constructor(private val signUpApi: SignUpApi) : SignUpRepository {
    override suspend fun signUpResponse(signUpEntity: SignUpEntity): Flow<DataState<WrappedResponse<SignUpApiResponse>>> = flow {
        emit(DataState.Success(signUpApi.getSignUpApiResponse("register-user",signUpEntity)))
    }
}