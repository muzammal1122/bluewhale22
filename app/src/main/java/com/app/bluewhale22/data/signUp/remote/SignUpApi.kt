package com.app.bluewhale22.data.signUp.remote

import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.domain.signUp.entity.SignUpApiResponse
import com.app.bluewhale22.domain.signUp.entity.SignUpEntity
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Url

interface SignUpApi {

    @POST
    suspend fun getSignUpApiResponse(@Url url:String, @Body body:SignUpEntity) :  WrappedResponse<SignUpApiResponse>
}