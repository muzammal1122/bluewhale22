package com.app.bluewhale22.data.referral.remote

import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.domain.referral.entity.ReferralApiResponse
import com.app.bluewhale22.domain.referral.entity.ReferralCodeResponse
import com.app.bluewhale22.domain.referral.entity.ReferralStatusEntity
import com.app.bluewhale22.domain.referral.entity.ReferralStatusResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Url

interface ReferralApi {

    @GET
    suspend fun getReferralCode(@Url url :String) : WrappedResponse<ReferralApiResponse>

    @GET
    suspend fun getMyReferralCode(@Url url :String) : WrappedResponse<ReferralCodeResponse>

    @PUT
    suspend fun getMyReferralStatus(@Url url :String, @Body body : ReferralStatusEntity) : WrappedResponse<ReferralStatusResponse>
}