package com.app.bluewhale22.data.referral

import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.data.referral.remote.ReferralApi
import com.app.bluewhale22.domain.referral.ReferralRepo
import com.app.bluewhale22.domain.referral.entity.ReferralApiResponse
import com.app.bluewhale22.domain.referral.entity.ReferralCodeResponse
import com.app.bluewhale22.domain.referral.entity.ReferralStatusEntity
import com.app.bluewhale22.domain.referral.entity.ReferralStatusResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class ReferralRepoImp @Inject constructor(val api: ReferralApi) : ReferralRepo {
    override suspend fun getReferralCode(url: String): Flow<DataState<WrappedResponse<ReferralApiResponse>>> =
        flow {
            emit(DataState.Success(api.getReferralCode("get_referral_code/$url")))
        }

    override suspend fun getMyReferralCode(url: String): Flow<DataState<WrappedResponse<ReferralCodeResponse>>> =
        flow {
            emit(DataState.Success(api.getMyReferralCode("get_active_referral_code")))
        }

    override suspend fun getReferralStatus(
        url: String,
        entity: ReferralStatusEntity
    ): Flow<DataState<WrappedResponse<ReferralStatusResponse>>> = flow {
        emit(DataState.Success(api.getMyReferralStatus("update_user_referral_details", entity)))
    }


}