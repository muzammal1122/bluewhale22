package com.app.bluewhale22.data.dashboard

import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.data.dashboard.remote.DashBoardApi
import com.app.bluewhale22.domain.dashboard.DashBoardApiRepo
import com.app.bluewhale22.domain.dashboard.entity.DashBoardApiResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class DashBoardApiRepoImp @Inject constructor(private val dashBoardApi: DashBoardApi) : DashBoardApiRepo {
    override suspend fun getDashBoardApiResponse(url: String): Flow<DataState<WrappedResponse<DashBoardApiResponse>>> =
        flow {
            emit(DataState.Success(dashBoardApi.getDashBoardData("market_movement")))
        }
}