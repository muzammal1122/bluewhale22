package com.app.bluewhale22.data.home.remote

import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.domain.home.entity.AddMoreEntity
import com.app.bluewhale22.domain.home.entity.AddMoreResponse
import com.app.bluewhale22.domain.home.entity.HomeApiResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Url

interface HomeApi {

    @GET
    suspend fun getHomeApi(@Url url :String) : WrappedResponse<HomeApiResponse>

    @PUT
    suspend fun addMore(@Url url : String, @Body entity : AddMoreEntity) : WrappedResponse<AddMoreResponse>
}