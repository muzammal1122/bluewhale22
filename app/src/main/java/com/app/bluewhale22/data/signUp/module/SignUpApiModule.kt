package com.app.bluewhale22.data.signUp.module

import com.app.bluewhale22.data.signUp.SignupRepoImp
import com.app.bluewhale22.data.signUp.remote.SignUpApi
import com.app.bluewhale22.data.test.TestRepositoryImp
import com.app.bluewhale22.data.test.remote.TestApi
import com.app.bluewhale22.domain.signUp.SignUpRepository
import com.app.bluewhale22.domain.signUp.entity.SignUpApiResponse
import com.app.bluewhale22.domain.test.TestRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent :: class)
object SignUpApiModule{

    @Provides
    fun providesSignUpRepository(signUPApi: SignUpApi): SignUpRepository {
        return SignupRepoImp(signUPApi)
    }

    @Provides
    fun provideSignupApi(retrofit: Retrofit): SignUpApi {
        return retrofit.create(SignUpApi::class.java)
    }

}