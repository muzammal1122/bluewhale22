package com.app.bluewhale22.data.common.response

data class ResponseStatus (
    val status :String? = null,
    val message : String? = null,
  )