package com.app.bluewhale22.data.login.module

import com.app.bluewhale22.data.login.LoginRepositoryImp
import com.app.bluewhale22.data.login.remote.LoginApi
import com.app.bluewhale22.domain.login.LoginRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit


@Module
@InstallIn(SingletonComponent :: class)
object LoginApiModule {

    @Provides
    fun providesLoginRepository(loginApi: LoginApi) : LoginRepository {
        return LoginRepositoryImp(loginApi)
    }

    @Provides
    fun provideLoginApi(retrofit: Retrofit): LoginApi {
        return retrofit.create(LoginApi::class.java)
    }
}