package com.app.bluewhale22.data.test

import com.app.bluewhale22.data.common.utils.DataState
import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.data.test.remote.TestApi
import com.app.bluewhale22.domain.test.entity.TestApiResponse
import com.app.bluewhale22.domain.test.TestRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class TestRepositoryImp @Inject constructor(private val testApi:TestApi): TestRepository {
    override suspend fun testResponse(): Flow<DataState<WrappedResponse<TestApiResponse>>> = flow {
        emit(DataState.Success(testApi.getTestApiResponse("entries")))
    }

    override suspend fun xyz(): String {
        return "eetete"
    }
}