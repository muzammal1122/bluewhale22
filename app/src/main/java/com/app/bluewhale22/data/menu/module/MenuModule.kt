package com.app.bluewhale22.data.menu.module

import com.app.bluewhale22.data.menu.MenuRepoImpl
import com.app.bluewhale22.data.menu.remote.MenuApi
import com.app.bluewhale22.domain.menu.MenuRepo
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
object MenuModule {

    @Provides
    fun providesUpdateProfileRepository(api: MenuApi): MenuRepo {
        return MenuRepoImpl(api)
    }

    @Provides
    fun provideUpdateProfileApi(retrofit: Retrofit): MenuApi {
        return retrofit.create(MenuApi::class.java)
    }
}