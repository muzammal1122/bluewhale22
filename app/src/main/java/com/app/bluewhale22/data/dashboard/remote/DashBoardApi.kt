package com.app.bluewhale22.data.dashboard.remote

import com.app.bluewhale22.data.common.utils.WrappedResponse
import com.app.bluewhale22.domain.dashboard.entity.DashBoardApiResponse
import retrofit2.http.GET
import retrofit2.http.Url

interface DashBoardApi {

    @GET
    suspend fun getDashBoardData(@Url url: String): WrappedResponse<DashBoardApiResponse>
}