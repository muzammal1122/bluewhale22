package com.app.bluewhale22.data.test.module

import com.app.bluewhale22.data.test.TestRepositoryImp
import com.app.bluewhale22.data.test.remote.TestApi
import com.app.bluewhale22.domain.test.TestRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent :: class)
object TestApiModule{

    @Provides
    fun providesTestRepository(testApi:TestApi):TestRepository{
        return TestRepositoryImp(testApi)
    }

}


