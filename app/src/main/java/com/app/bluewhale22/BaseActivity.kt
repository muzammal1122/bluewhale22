package com.app.bluewhale22


import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.lifecycleScope

import com.app.bluewhale22.utils.ContextUtils

import dagger.hilt.android.AndroidEntryPoint

import kotlinx.coroutines.launch
import java.util.*

import androidx.datastore.core.DataStore
import com.app.bluewhale22.utils.SharedPrefs


import javax.inject.Inject

@AndroidEntryPoint
open class BaseActivity : AppCompatActivity() {
    @Inject
    lateinit var preferences: DataStore<Preferences>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }

    override fun attachBaseContext(newBase: Context?) {
        val langPref = SharedPrefs(newBase!!)
            val lang =langPref.getLanguage()
            val localeToSwitchTo = Locale(lang)
            ContextUtils.updateLocale(newBase, localeToSwitchTo)
           super.attachBaseContext(newBase)
    }

}