package com.app.bluewhale22

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import com.app.bluewhale22.presentation.test.TestViewModel
import dagger.hilt.android.AndroidEntryPoint
import androidx.lifecycle.lifecycleScope
import com.app.bluewhale22.domain.test.entity.TestApiResponse
import com.app.bluewhale22.presentation.test.TestFragmentStateModel
import com.app.bluewhale22.utils.gone
import com.app.bluewhale22.utils.showToast
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject lateinit var viewModel : TestViewModel
    val TAG = "MainActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initObserver()
        viewModel.getTestApiResponse()
        findViewById<TextView>(R.id.text).gone()

        startActivity(Intent(this, BaseActivity::class.java))
        launchActivity<BaseActivity>()


    }

    private fun initObserver(){
        viewModel.mState.flowWithLifecycle(
           this.lifecycle,Lifecycle.State.STARTED
        ).onEach {
            handleState(it)
        }.launchIn(this.lifecycleScope)
    }
    private fun handleState(state: TestFragmentStateModel){
        when(state){
            is TestFragmentStateModel.IsLoading -> handleIsLoadingState(state.isLoading)
            is TestFragmentStateModel.TestApiResponsee -> handleApiRespone(state.testApiResponse)
            is TestFragmentStateModel.FoundException -> handleException(state.exception)
            else -> {}
        }
    }


    private fun handleException(exception: Exception) {
        showToast(exception.message?:"Unknown exception")
    }

    private fun handleApiRespone(testApiResponse: TestApiResponse) {
        Log.i(TAG,""+testApiResponse)

    }

    private fun handleIsLoadingState(loading: Boolean) {
            Log.i(TAG,""+loading)
    }



    /**
    * Extension Function for launching new activity

    * */
    inline fun <reified T : Any> Activity.launchActivity (
        requestCode: Int = -1,
        options: Bundle? = null,
        noinline init: Intent.() -> Unit = {})
    {
        val intent = newIntent<T>(this)
        intent.init()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
        {
            startActivityForResult(intent, requestCode, options)
        } else {
            startActivityForResult(intent, requestCode)
        }
    }

    inline fun <reified T : Any> Context.launchActivity (
        options: Bundle? = null,
        noinline init: Intent.() -> Unit = {})
    {
        val intent = newIntent<T>(this)
        intent.init()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
        {
            startActivity(intent, options)
        } else {
            startActivity(intent)
        }
    }

    inline fun <reified T : Any> newIntent(context: Context): Intent =
        Intent(context, T::class.java)

    fun onTextClick(view: View) {
        launchActivity<BaseActivity>()
    }
}